
[![en](https://img.shields.io/badge/lang-en-red.svg)](https://gitlab.com/Askanimus/arbeitszeitkonto/-/blob/master/README.md)
[![es](https://img.shields.io/badge/lang-it-yellow.svg)](https://gitlab.com/Askanimus/arbeitszeitkonto/-/blob/master/README.it.md)

Arbeitszeitkonto
--------------------

Das „Arbeitszeitkonto“ dient dazu, Ihre persönliche Arbeits- und Freizeit zu erfassen und übersichtlich aufzubereiten.
Es können mehrere Arbeitsplätze verwaltet werden, damit bei einem Arbeitsplatzwechsel alte Daten nicht gelöscht werden müssen oder um parallele Arbeitsplätze/Projekte getrennt zu verwalten.

Für die Ansicht auf dem Mobiltelefon stehen eine Tages-, Wochen-, Monats- und Jahresansicht zur Verfügung, die auch als jeweilige Standardansichten definiert werden können.

Für jeden Arbeitsplatz können Sie ein Widget zum Stempeln und Entstempeln Ihrer Arbeitszeiten erstellen.

Zusätzlich zur Anzeige auf Ihrem Bildschirm können Sie Wochen-, Monats- oder Jahresberichte als PDF- oder CSV-Datei erstellen. Die generierten Berichte können per E-Mail oder Messenger verschickt oder an eine Drucker-App übergeben werden.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/askanimus.arbeitszeiterfassung2/)
[<img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png"
alt="Get it on IzzyOnDroid"
height="80">](https://apt.izzysoft.de/fdroid/index/apk/askanimus.arbeitszeiterfassung2)

Oder Download der aktuellen Version der APK Datei unter [Releases Section](https://gitlab.com/Askanimus/arbeitszeitkonto/-/releases).