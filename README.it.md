
[![en](https://img.shields.io/badge/lang-en-red.svg)](https://gitlab.com/Askanimus/arbeitszeitkonto/-/blob/master/README.md)
[![pt-br](https://img.shields.io/badge/lang-de-green.svg)](https://gitlab.com/Askanimus/arbeitszeitkonto/-/blob/master/README.de.md)

Conto ore di lavoro
--------------------

“Conto ore di lavoro” serve a registrare il tempo di lavoro e il tempo libero personali e a prepararli in modo chiaro.
È possibile gestire più postazioni di lavoro in modo da non dover cancellare i vecchi dati quando si cambia lavoro o per gestire separatamente lavori/progetti paralleli.

È disponibile una vista giornaliera, settimanale, mensile e annuale per la visualizzazione su un telefono cellulare, che può anche essere definita come la rispettiva vista standard.

È possibile creare un widget per ogni postazione di lavoro per timbrare e deselezionare le ore di lavoro.

Oltre alla visualizzazione sullo schermo, è possibile generare rapporti settimanali, mensili o annuali come file PDF o CSV. I rapporti generati possono essere inviati via e-mail o messenger o trasferiti a un'applicazione di stampa.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/askanimus.arbeitszeiterfassung2/)
[<img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png"
     alt="Get it on IzzyOnDroid"
     height="80">](https://apt.izzysoft.de/fdroid/index/apk/askanimus.arbeitszeiterfassung2)


Oppure scaricate l'ultimo APK dal sito [Releases Section](https://gitlab.com/Askanimus/arbeitszeitkonto/-/releases).