Arbeitszeitkonto dient dazu, Ihre persönlichen Arbeits- und Freizeiten zu erfassen und übersichtlich aufzubereiten.

<b>Was kann Arbeitszeitkonto?</b>

- Arbeitszeiten erfassen
- Überstunden berechnen
- Abwesenheiten (Krank, Urlaub etc.) zu dokumentieren
- beliebige Zusatzwerte (Einsatzorte, Notizen, Spesen Fahrzeiten etc.) erfassen und in die Berechnung von Verdienst und Stunden einbeziehen
- Urlaubstage verwalten
- verschiedene Übersichten (Jahr, Monat, Woche und Tag) mit Arbeitszeitberechnung in der App anzeigen
- konfigurierbare Berichte als PDF, CSV und iCal Dateien erzeugen und an Drucker-, Mail- oder Messanger Apps weitergeben
- mehrere Arbeitsplätze, Projekte oder Auftraggeber verwalten
- Datensicherungen anlegen (auch autom. wenn gewünscht) und wiederherstellen
- zwei Schichtmodelle (Wechselschicht und Teilschicht)
- mehrere Arbeitszeitmodelle (Rollende Woche, Monatsstunden, 5 Tage Woche, 6 Tage Woche, x Tage Woche)
- Stunden mit NFC stempeln

In der App befindet sich ein Handbuch als PDF Datei, diese erklärt alle Funktionen der App und führt in die Arbeitszeitberechnung ein.


<b>Was kann die App nicht</b>

- Werbung anzeigen
- Katzenvideos oder -bilder anzeigen
- Ihre Daten an dritte weitergeben
- im Internet Bestellungen aufgeben
- die Welt retten


<b>Danke an</b>

- alle, die mir mit Fehlerberichten, Vorschlägen und Kritik geholfen haben
- Freepik.com & dr. Web für die Human Pictos
- das Team "Code-Troopers" für Ihr Projekt "BetterPickers"
- die Teams von IzzySoft und F-Droid für ihre Unterstützung bei der Veröffentlichung


<b>Benötigte Rechte</b>

- schreiben von Dateien zum Speichern der Berichte und Datenbanksicherungen
- lesen von Dateien zum wiederherstellen von Datensicherungen


Bitte beachten Sie, das ich keine Haftung für das korrekte funktionieren der Software oder die eingegebenen/berechneten Zeiten übernehme.

Bei Fehlfunktionen oder anderen Merkwürdigkeiten melden Sie sich bitte bei mir (askanimus@gmail.com) damit wir gemeinsam eine Lösung finden.