[![pt-br](https://img.shields.io/badge/lang-de-green.svg)](https://gitlab.com/Askanimus/arbeitszeitkonto/-/blob/master/README.de.md)
[![es](https://img.shields.io/badge/lang-it-yellow.svg)](https://gitlab.com/Askanimus/arbeitszeitkonto/-/blob/master/README.it.md)

Working Time Account
--------------------

"Working Time Account" is used to record your personal working and free time and to prepare it clearly.
Several workstations can be managed so that old data does not have to be deleted when changing jobs or to manage parallel jobs/projects separately.

A daily, weekly, monthly and yearly view is available for viewing on a mobile phone, which can also be defined as the respective standard views.

You can create a widget for each workstation to stamp and unstamp your working hours.

In addition to the display on your screen, you can generate weekly, monthly or annual reports as PDF or CSV files. The generated reports can be sent by email or messenger or transferred to a printer app.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/askanimus.arbeitszeiterfassung2/)
[<img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png"
alt="Get it on IzzyOnDroid"
height="80">](https://apt.izzysoft.de/fdroid/index/apk/askanimus.arbeitszeiterfassung2)

Or download the latest APK from the [Releases Section](https://gitlab.com/Askanimus/arbeitszeitkonto/-/releases).