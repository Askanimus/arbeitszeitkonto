/* Copyright 2014-2023 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import android.app.Activity;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.preference.PreferenceManager;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.datenbank.AUpdateDatenbank;


public class SettingsActivity
        extends
        AppCompatActivity
        implements
        ISettings,
        View.OnClickListener{
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager2 mViewPager;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private AppCompatButton mButtonBack;
    private AppCompatButton mButtonNext;

    private Arbeitsplatz mArbeitsplatz = null;
    ArrayList<SettingsTab> settingsTabs;
    private boolean isNfc;
    private boolean isEndeAufzeichnung;
    private boolean isNurJob;
    private boolean isInitAssist;
    /*
     wird zum anpassen der App Sprache benötigt, wenn diese von der Systemsprache abweicht
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getBoolean(ASettings.KEY_THEMA_DUNKEL, false) ?
                        R.style.MyFullscreenTheme :
                        R.style.MyFullscreenTheme_Light
        );
        setContentView(R.layout.activity_settings);

        // ermitteln ob NFC Hardware vorhanden ist
        isNfc = NfcAdapter.getDefaultAdapter(getApplicationContext()) != null;

        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                BackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String mAction = getIntent().getAction();
        if(APP_RESET.equals(mAction)){
            ASettings.zustand=INIT_ZUSTAND_UNGELADEN;
        }
        ASettings.init(this, this::resume);
    }

    void resume() {
        Intent intent = getIntent();

        long arbeitsplatzID = intent.getLongExtra(ISettings.KEY_EDIT_JOB, 0);
        if (arbeitsplatzID == 0) {
            arbeitsplatzID = ASettings.mPreferenzen.getLong(ISettings.KEY_EDIT_JOB, 0);
        }
        mArbeitsplatz = ASettings.getArbeitsplatz(arbeitsplatzID);

        if (mArbeitsplatz != null) {
            int startSeite = intent.getIntExtra(ISettings.KEY_INIT_SEITE, ISettings.SETUP_SEITE_ALLGEMEIN);

            // zeigt ob es Ersteinrichtung der App(oder eines Arbeitsplatzes) ist
            isInitAssist = intent.getBooleanExtra(ISettings.ARG_IS_INITASSIST, false);

            // sind es nur die Einstellungen des Arbeitsplatzes
            isNurJob = intent.getBooleanExtra(ISettings.ARG_NUR_JOB, false);

            // welcher Arbeitsplatz wird editiert
            ASettings.mPreferenzen.edit().putLong(ISettings.KEY_EDIT_JOB, mArbeitsplatz.getId()).apply();

            // Bei Aufzeichnungsende Anzeige der Oprtionen reduzieren
            if (mArbeitsplatz.isEndeAufzeichnung(ASettings.aktDatum)) {
                //mSeiten = MAX_SEITEN_AUFZEICHNUNGSENDE;
                isEndeAufzeichnung = true;
                openHinweis();
            } else {
                //mSeiten = MAX_SEITEN_ALL;
                isEndeAufzeichnung = false;
            }


            // Anzeigeelemente finden
            mToolbar = findViewById(R.id.S_toolbar);
            mViewPager = findViewById(R.id.S_container);
            mTabLayout = findViewById(R.id.S_tabs);
            mButtonNext = findViewById(R.id.S_knopf_vor);
            mButtonBack = findViewById(R.id.S_knopf_zurueck);

            setSupportActionBar(mToolbar);
            mToolbar.setBackgroundColor(mArbeitsplatz.getFarbe());
            mToolbar.setTitleTextColor(mArbeitsplatz.getFarbe_Schrift_Titel());

            if (!isInitAssist) {
                // dafür die Navigationsknölpfe verbergen
                findViewById(R.id.S_navigation).setVisibility(View.GONE);
                // Toolbar und Tabs zeigen weil es die Einstellungen sind
                mTabLayout.setSelectedTabIndicatorColor(mArbeitsplatz.getFarbe());
                mToolbar.setTitle(ASettings.res.getString(R.string.title_activity_settings));
            } else {
                // Toolbar und Tabs verbergen weil es die Ersteinrichtung der App bzw. eines neuen Arbeitsplatzes ist
                mTabLayout.setVisibility(View.GONE);
                if (isNurJob) {
                    // die Einrichtung eines neuen Arbeitsplatz
                    mToolbar.setTitle(ASettings.res.getString(R.string.title_activity_init_neuer_arbeitsplatz));
                } else {
                    // die Ersteinrichtung dert App
                    mToolbar.setTitle(ASettings.res.getString(R.string.title_activity_init_assistent));
                }

                // das Aussehen der Knöpfe anpassen
                ViewCompat.setBackgroundTintList(mButtonNext, mArbeitsplatz.getFarbe_Button());
                ViewCompat.setBackgroundTintList(mButtonBack, mArbeitsplatz.getFarbe_Button());

                mButtonBack.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());
                mButtonNext.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());

                // die Navigationsknlpfe beleben
                mButtonNext.setOnClickListener(this);
                mButtonBack.setOnClickListener(this);
            }

            // die anzuzeigenden Tabs anpassen
            settingsTabs = new ArrayList<>();

            if(intent.getBooleanExtra(ISettings.ARG_NUR_BACKUP, false)){
                startSeite = 0;
                settingsTabs.add(new SettingsTab(
                        ASettings.res.getStringArray(R.array.prefs_tabs)[SETUP_SEITE_DATENSICHERUNG],
                        SETUP_SEITE_DATENSICHERUNG
                ));
            } else {
                int pos = 0;
                for (String tab : ASettings.res.getStringArray(R.array.prefs_tabs)) {
                    switch (pos) {
                        case SETUP_SEITE_MIGRATION:
                            if(!ASettings.mPreferenzen.contains(KEY_INIT_FINISH)) {
                                settingsTabs.add(new SettingsTab(tab, pos));
                            }
                            break;
                        case SETUP_SEITE_ALLGEMEIN:
                        case SETUP_SEITE_DATENSICHERUNG:
                            if (!isNurJob) {
                                settingsTabs.add(new SettingsTab(tab, pos));
                            }
                            break;
                        case SETUP_SEITE_ARBEITSPLATZ:
                        case SETUP_SEITE_AUFZEICHNUNG:
                            settingsTabs.add(new SettingsTab(tab, pos));
                            break;
                        case SETUP_SEITE_NFC:
                            if (isNfc) {
                                settingsTabs.add(new SettingsTab(tab, pos));
                            }
                            break;
                        default:
                            if (!isEndeAufzeichnung) {
                                settingsTabs.add(new SettingsTab(tab, pos));
                            }
                    }
                    if (startSeite == pos) {
                        startSeite = settingsTabs.size() - 1;
                    }
                    pos++;
                }
            }

            // Den Adapter anlegen, der die gewünschte Einstellungen Seite liefert
            mSectionsPagerAdapter = new SectionsPagerAdapter(
                    getSupportFragmentManager(),
                    getLifecycle()
            );

            // Den Viewpager einrichten.
            mViewPager.setAdapter(mSectionsPagerAdapter);

            if (!isInitAssist) {
                TabLayoutMediator mTabMediator = new TabLayoutMediator(
                        mTabLayout,
                        mViewPager,
                        true,
                        true,
                        (tab, position) -> tab.setText(settingsTabs.get(position).name));
                mTabMediator.attach();
            }

            mViewPager.setCurrentItem(startSeite,false);

            // alten Zustand der App wieder anzeigen, nach Rückkehr aus den Einstellungen
            ASettings.mPreferenzen.edit().putBoolean(ISettings.KEY_RESUME_VIEW, true).apply();

            if(isInitAssist){
                if(mViewPager.getCurrentItem() == 0){
                    mButtonBack.setVisibility(View.INVISIBLE);
                } else if(mViewPager.getCurrentItem() == settingsTabs.size() -1){
                    mButtonNext.setText(R.string.button_end);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        int seite = mViewPager.getCurrentItem();
        if (v == mButtonNext) {
            seite++;
            if (seite == settingsTabs.size()) {

                // den Initprozess als beendet markieren
                ASettings.mPreferenzen.edit().putBoolean(ISettings.KEY_INIT_FINISH, true).apply();

                // Die Monate und Jahre vom Aufzeichnungsbeginn bis heute anlegen und
                // mit Soll, Ist, Saldo, Saldo des Vormonat vorbelegen
                AUpdateDatenbank.updateDatenbank(
                        this,
                        mArbeitsplatz
                );
            }
        } else if (v == mButtonBack) {
            seite--;
        }
        mViewPager.setCurrentItem(seite);
        if (seite == 0) {
            mButtonBack.setVisibility(View.INVISIBLE);
        } else {
            mButtonBack.setVisibility(View.VISIBLE);
            if (seite == settingsTabs.size() - 1) {
                mButtonNext.setText(R.string.button_end);
            }
        }
    }

    private void BackPressed() {
        if (isInitAssist) {
            if (mViewPager.getCurrentItem() == 0) {
                // es ist die erste Seite der Einstellungen
                // den Arbeitspltz doch nicht anlegen
                if (ASettings.mPreferenzen.contains(ISettings.KEY_INIT_FINISH)) {
                    mArbeitsplatz.delete();
                    finish();
                }
            } else {
                // eine Seite zurück
                onClick(mButtonBack);
            }
        } else {
            // das Fragment für NFC Einstellungen ist nicht mehr offen
            ASettings.mPreferenzen.edit().putBoolean(ISettings.KEY_NFC_EDIT, false).apply();

            // alten Zustand der App wieder anzeigen
            SharedPreferences.Editor mEdit = ASettings.mPreferenzen.edit();
            mEdit.putBoolean(ISettings.KEY_RESUME_VIEW, true).apply();

            if (mArbeitsplatz != null) {
                if (mArbeitsplatz.istGeaendert()) {
                    mArbeitsplatz.schreibeJob();
                    requestBackup();
                }
                // Die Monate und Jahre vom Aufzeichnungsbeginn bis heute anlegen/ aktuallisieren und
                // mit Soll, Ist, Saldo, Saldo des Vormonat vorbelegen
                if (mArbeitsplatz.istNeuberechnung()) {
                    mArbeitsplatz.resetNeuberechnung();
                    AUpdateDatenbank.updateDatenbank(
                            this,
                            mArbeitsplatz);
                } else {
                    Intent mMainIntent = new Intent();
                    mMainIntent.setClass(this, MainActivity.class);
                    mMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mMainIntent.setAction(ISettings.APP_RESET);
                    finish();
                    startActivity(mMainIntent);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        if(isChangingConfigurations()) {
            Intent mSettingsIntent = new Intent();
            mSettingsIntent.setClass(this, this.getClass());
            if(mArbeitsplatz != null) {
                mSettingsIntent.putExtra(ISettings.KEY_EDIT_JOB, mArbeitsplatz.getId());
            }
            mSettingsIntent.putExtra(ISettings.KEY_INIT_SEITE, mViewPager.getCurrentItem());
            mSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mSettingsIntent);
        } else {
            if(mArbeitsplatz != null) {
                ASettings.mPreferenzen.edit()
                        .putLong(ISettings.KEY_EDIT_JOB, mArbeitsplatz.getId())
                        .apply();
            }
            if(mViewPager != null) {
                ASettings.mPreferenzen.edit()
                        .putInt(ISettings.KEY_INIT_SEITE, mViewPager.getCurrentItem())
                        .apply();
            }
            ASettings.zustand= ISettings.INIT_ZUSTAND_UNGELADEN; // neuladen der Einstellungen erzwingen
        }
        super.onDestroy();
    }

    /*
     * Rückmeldung vom Rechtemanagment nach Rechteanfrage
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
            requestCode = requestCode & 0x0000ffff;
            if (requestCode == REQ_DEMAND_WRITE) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // Recht verweigert
                    Toast.makeText(
                            this,
                            getString(R.string.err_keine_berechtigung),
                            Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    // wird nach Auswahl eines Verzeichnisses aufgerufen
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            requestCode &= 0x0000ffff;
            Uri treeUri = data.getData();
            if (treeUri != null) {
                Intent intent = getIntent();
                if (requestCode == REQ_FOLDER_PICKER_WRITE_EXPORT) {
                    getContentResolver().takePersistableUriPermission(
                            treeUri,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                    ASettings.mPreferenzen.edit()
                            .putString(ASettings.KEY_DATEN_DIR, treeUri.toString())
                            .apply();

                    intent.putExtra(ISettings.KEY_INIT_SEITE, ISettings.SETUP_SEITE_ALLGEMEIN);

                }  else if (requestCode == REQ_FOLDER_PICKER_WRITE_BACKUP) {
                    getContentResolver().takePersistableUriPermission(
                            treeUri,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                    ASettings.mPreferenzen.edit()
                            .putString(ASettings.KEY_BACKUP_DIR, treeUri.toString())
                            .apply();

                    intent.putExtra(ISettings.KEY_INIT_SEITE, ISettings.SETUP_SEITE_DATENSICHERUNG);

                }  else if (requestCode == REQ_FOLDER_PICKER_READ) {
                    getContentResolver().takePersistableUriPermission(
                            treeUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                    );

                    ASettings.mPreferenzen.edit()
                            .putString(ASettings.KEY_IMPORT_DIR, treeUri.toString())
                            .apply();

                } else if (requestCode == REQ_IMAGE_READ) {
                    // Unterschriftscan kopieren
                    InputStream in;
                    OutputStream out;
                    try {
                        in = getContentResolver().openInputStream(treeUri);
                        out = getContentResolver().openOutputStream(
                                Uri.fromFile(new File(getFilesDir(), "UnterschriftAN.jpg"))
                        );

                        if (out != null && in != null) {
                            byte[] buffer = new byte[1024];
                            int read;
                            while ((read = in.read(buffer)) != -1) {
                                out.write(buffer, 0, read);
                            }
                            in.close();
                            out.flush();
                            out.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * A {@link FragmentStateAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentStateAdapter /*implements
            InitAssistentFragmentAufzeichnung.InitAufzeichnungCallbacks*/
            /*InitAssistentFragmentArbeitsplatz.InitArbeitsplatzCallbacks*/{

         /*Fragment[] fragment = new Fragment[]{
                 InitAssistentFragmentAllgemein.newInstance(mArbeitsplatz.getId()),
                 InitAssistentFragmentDatensicherung.newInstance(mArbeitsplatz.getId()),
                 InitAssistentFragmentArbeitsplatz.newInstance(mArbeitsplatz.getId(), false),
                 InitAssistentFragmentAufzeichnung.newInstance(mArbeitsplatz.getId(), true),
                 InitAssistentFragmentZusatzeintraege.newInstance(mArbeitsplatz.getId()),
                 InitAssistentFragmentArbeitszeit.newInstance(mArbeitsplatz.getId()),
                 InitAssistentFragmentSchicht.newInstance(mArbeitsplatz.getId()),
                 InitAssistentFragmentNfc.newInstance(mArbeitsplatz.getId(),getIntent().getLongExtra(KEY_NFC_TAG, 0)),
                 InitAssistentFragmentAbwesenheit.newInstance(mArbeitsplatz.getId())
        };*/

         ArrayList<Fragment> fragmente = new ArrayList<>();


        SectionsPagerAdapter(FragmentManager fm, Lifecycle l) {
            super(fm, l);

            for (SettingsTab tab : settingsTabs ) {
                switch (tab.setting) {
                    case SETUP_SEITE_MIGRATION:
                        fragmente.add(SettingsFragmentMigration.newInstance());
                        break;
                    case SETUP_SEITE_ALLGEMEIN :
                        fragmente.add(SettingsFragmentAllgemein.newInstance(mArbeitsplatz.getId(), isInitAssist));
                        break;
                    case SETUP_SEITE_DATENSICHERUNG:
                        fragmente.add(SettingsFragmentDatensicherung.newInstance(mArbeitsplatz.getId(), isInitAssist));
                        break;
                    case  SETUP_SEITE_ARBEITSPLATZ:
                        fragmente.add(SettingsFragmentArbeitsplatz.newInstance(mArbeitsplatz.getId(), false, isInitAssist));
                        break;
                    case SETUP_SEITE_AUFZEICHNUNG:
                        fragmente.add(SettingsFragmentAufzeichnung.newInstance(mArbeitsplatz.getId(), isInitAssist));
                        break;
                    case SETUP_SEITE_ZUSATZ:
                        fragmente.add(SettingsFragmentZusatzeintraege.newInstance(mArbeitsplatz.getId(), isInitAssist));
                        break;
                    case SETUP_SEITE_ARBEITSZEIT:
                        fragmente.add(SettingsFragmentArbeitszeit.newInstance(mArbeitsplatz.getId(), isInitAssist));
                        break;
                    case SETUP_SEITE_SCHICHTEN:
                        fragmente.add(SettingsFragmentSchicht.newInstance(mArbeitsplatz.getId(), isInitAssist));
                        break;
                    case SETUP_SEITE_NFC:
                        fragmente.add(SettingsFragmentNfc.newInstance(mArbeitsplatz.getId(),getIntent().getLongExtra(KEY_NFC_TAG, 0), isInitAssist));
                        break;
                    case SETUP_SEITE_ABWESENHEITEN:
                        fragmente.add(SettingsFragmentAbwesenheit.newInstance(mArbeitsplatz.getId(), isInitAssist));
                        break;
                }
            }
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            Fragment mAktuellOffen;
            mAktuellOffen = fragmente.get(position);
            /*if(position == 3){
                ((InitAssistentFragmentAufzeichnung) mAktuellOffen).setCallback(this);
            }*/

            /*switch (position) {
                case 0:
                    mAktuellOffen = InitAssistentFragmentAllgemein.newInstance(mArbeitsplatz.getId());
                    break;
                case 1:
                    mAktuellOffen = InitAssistentFragmentDatensicherung.newInstance(mArbeitsplatz.getId());
                    break;
                case 2:
                    mAktuellOffen = InitAssistentFragmentArbeitsplatz.newInstance(mArbeitsplatz.getId(), false);
                    //((InitAssistentFragmentArbeitsplatz) mAktuellOffen).setCallback(this);
                    break;
                case 3:
                    mAktuellOffen = InitAssistentFragmentAufzeichnung.newInstance(mArbeitsplatz.getId(), true);
                    ((InitAssistentFragmentAufzeichnung) mAktuellOffen).setCallback(this);
                    break;
                case 4:
                    mAktuellOffen = InitAssistentFragmentZusatzeintraege.newInstance(mArbeitsplatz.getId());
                    break;
                case 5:
                    mAktuellOffen = InitAssistentFragmentArbeitszeit.newInstance(mArbeitsplatz.getId());
                    break;
                case 6:
                    mAktuellOffen = InitAssistentFragmentSchicht.newInstance(mArbeitsplatz.getId());
                    break;
                case 7:
                    mAktuellOffen = InitAssistentFragmentNfc.newInstance(
                            mArbeitsplatz.getId(),
                            getIntent().getLongExtra(KEY_NFC_TAG, 0)
                    );
                    break;
                default:
                    mAktuellOffen = InitAssistentFragmentAbwesenheit.newInstance(mArbeitsplatz.getId());
                    break;
            }*/
            return mAktuellOffen;
        }

        @Override
        public int getItemCount() {
            return fragmente.size();
        }

        /*@Override
        public void onEndeAufzeichnungChanged(Boolean isEnde) {
            if(isEnde ) {
                if(mTabLayout.getTabCount() > MAX_SEITEN_AUFZEICHNUNGSENDE) {
                    mTabLayout.removeTabAt(8);
                    mTabLayout.removeTabAt(7);
                    mTabLayout.removeTabAt(6);
                    mTabLayout.removeTabAt(5);
                    mTabLayout.removeTabAt(4);
                    openHinweis();
                    mSeiten = MAX_SEITEN_AUFZEICHNUNGSENDE;
                    notifyDataSetChanged();
                }
            } else {
                if(mTabLayout.getTabCount() <= MAX_SEITEN_AUFZEICHNUNGSENDE) {
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.zusatz_eingaben), 4, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.arbeitszeit), 5, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.schichten), 6, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.nfc), 7, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.abwesenheiten), 8, false);
                    mSeiten = MAX_SEITEN_ALL;
                    notifyDataSetChanged();
                }
            }
        }*/

       /* @Override
        public void onSettingChaged(String schluessel, int wert){
            if (Datenbank.DB_F_FARBE.equals(schluessel)) {
                mToolbar.setBackgroundColor(wert);
                mToolbar.setTitleTextColor(mArbeitsplatz.getFarbe_Schrift_Titel());
                mTabLayout.setSelectedTabIndicatorColor(mArbeitsplatz.getFarbe());
            }
            notifyDataSetChanged();
        }*/
    }


    protected void openHinweis(){
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.aufzeichnung_ende))
                .setMessage(getString(R.string.dialog_aufzeichn_ende,
                        mArbeitsplatz.getEndDatum().getString_Datum(getApplicationContext())))
                .setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {
                    // nichts tun
                }).show();
    }

    // Backup im Google Konto anfordern
    public void requestBackup() {
        BackupManager bm = new BackupManager(this);
        try {
            bm.dataChanged();
        } catch (NullPointerException ne){
            ne.printStackTrace();
        }
    }

    private  class SettingsTab {
                String name;
                int setting;

                SettingsTab(String n, int s){
                    name = n;
                    setting = s;
                }
    }
}
