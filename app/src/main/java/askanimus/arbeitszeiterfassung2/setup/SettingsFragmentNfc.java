/* Copyright 2014-2023 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.nfc.NfcBekannterTag;
import askanimus.arbeitszeiterfassung2.nfc.NfcHelperActivity;
import askanimus.arbeitszeiterfassung2.nfc.NfcTagListe;
import askanimus.arbeitszeiterfassung2.nfc.NfcTagViewAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class SettingsFragmentNfc extends Fragment implements
        SwitchCompat.OnCheckedChangeListener,
        NfcTagViewAdapter.ItemClickListener {

    private Arbeitsplatz mArbeitsplatz;
    private NfcTagListe mNfcTagListe;

    private Context mContext;
    private TextView hNfcAktiv;
    private SwitchCompat sNfcAktiv;
    private TextView tListeTitel;
    private NfcTagViewAdapter mNfcTagViewAdapter;
    private RecyclerView mNfcTagListeView;
    private boolean isInitAssist;


    /*
     * Neue Instanz anlegen
     */
    public static SettingsFragmentNfc newInstance(@NonNull long arbeitsplatz, long tagId, boolean isInitAssist) {
        Bundle argumente = new Bundle();
        argumente.putLong(ISettings.KEY_EDIT_JOB, arbeitsplatz);
        argumente.putBoolean(ISettings.ARG_IS_INITASSIST, isInitAssist);
        if(tagId != 0) {
            argumente.putLong(ISettings.KEY_NFC_TAG, tagId);
        }
        SettingsFragmentNfc fragment = new SettingsFragmentNfc();
        fragment.setArguments(argumente);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getContext();

        View view = inflater.inflate(R.layout.fragment_init_nfc, container, false);

        mNfcTagListeView = view.findViewById(R.id.I_nfc_liste);
        mNfcTagViewAdapter = new NfcTagViewAdapter();
        GridLayoutManager layoutManger =
                new GridLayoutManager(
                        mContext,
                        1);
        mNfcTagListeView.setLayoutManager(layoutManger);
        mNfcTagListeView.setAdapter(mNfcTagViewAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ASettings.init(mContext, this::resume);
    }

    private void resume() {
        Bundle args = getArguments();
        View mView = getView();
        int openTag = 0;
        if (mView != null && args != null) {
            // den zu bearbeitenden Arbeitsplatz ermitteln
            mArbeitsplatz = ASettings.getArbeitsplatz(
                    args.getLong(ISettings.KEY_EDIT_JOB, 0)
            );
            isInitAssist = args.getBoolean(ISettings.ARG_IS_INITASSIST, false);

            if (mArbeitsplatz != null) {
                // NFC Editmodus aktivieren
                ASettings.mPreferenzen.edit().putBoolean(ISettings.KEY_NFC_EDIT, true).apply();

                // die Tagliste des Arbeitsplatzes lesen
                mNfcTagListe = new NfcTagListe(mArbeitsplatz.getId());

                // wenn die Seite mit einer NFC ID aufgerufen wurde, diesen Tag anzeigen/neu anlegen
                if (args.containsKey(ISettings.KEY_NFC_TAG)) {
                    openTag = mNfcTagListe.addTag(
                            args.getLong(ISettings.KEY_NFC_TAG, 0),
                            mArbeitsplatz.getId()
                    );
                }

                // Anzeigeelemente finden
                hNfcAktiv = mView.findViewById(R.id.I_hint_nfc);
                sNfcAktiv = mView.findViewById(R.id.I_switch_nfc);
                tListeTitel = mView.findViewById(R.id.I_nfc_listtitel);
                mNfcTagListeView = mView.findViewById(R.id.I_nfc_liste);
                //ImageView bAdd = mView.findViewById(R.id.I_add_schicht);


                // Seitentitel ausblenden wenn es nicht der Initassistent ist
                if (!isInitAssist) {
                    TextView tTitel = mView
                            .findViewById(R.id.I_nfc_titel);
                    tTitel.setVisibility(View.GONE);
                }

                // Knopffarben
                sNfcAktiv.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
                sNfcAktiv.setTrackTintList(mArbeitsplatz.getFarbe_Trak());

                // Handler und Adapter definieren
                //bAdd.setOnClickListener(this);
                sNfcAktiv.setOnCheckedChangeListener(this);

                // Die Tagliste anzeigen
                //setNfcListAdapter();
                mNfcTagViewAdapter.setUp(
                        mContext,
                        mArbeitsplatz,
                        this,
                        mNfcTagListe
                );
                mNfcTagViewAdapter.openItem(Math.max(openTag, 0));

                // Werte vorbelegen
                sNfcAktiv.setChecked(mArbeitsplatz.isNfcAktiv());

                updateView();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void updateView() {
        if (sNfcAktiv.isChecked()) {
            hNfcAktiv.setText(R.string.nfc_verwenden_hint);
            tListeTitel.setVisibility(View.VISIBLE);
            mNfcTagListeView.setVisibility(View.VISIBLE);
        } else {
            hNfcAktiv.setText(R.string.nfc_verwenden_no_hint);
            tListeTitel.setVisibility(View.INVISIBLE);
            mNfcTagListeView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onStop() {
        if (mArbeitsplatz != null) {
            mArbeitsplatz.schreibeJob();
        }
        super.onStop();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(mArbeitsplatz.isNfcAktiv() != isChecked) {
            mArbeitsplatz.setOption(Arbeitsplatz.OPT_NFC_AKTIV, isChecked);
            mArbeitsplatz.schreibeJob();
            ASettings.mPreferenzen.edit()
                    .putBoolean(ISettings.KEY_NFC_AKTIV + mArbeitsplatz.getId(), isChecked)
                    .apply();

            Intent nfcHelperIntent = new Intent();
            nfcHelperIntent.setClass(mContext.getApplicationContext(), NfcHelperActivity.class);
            nfcHelperIntent.setAction(
                    isChecked ? ISettings.ACTION_NFC_AKTIVIEREN : ISettings.ACTION_NFC_DEAKTIVIEREN
            );
            nfcHelperIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(nfcHelperIntent);
        } else {
            updateView();
        }
    }

    /*@Override
    public void onNfcTagDelete(int index) {
        SchichtDefault sd = mArbeitsplatz.getDefaultSchichten().getAktive(index);
        new AlertDialog.Builder(mContext)
                .setTitle(mContext.getString(R.string.dialog_delete, sd.getName()))
                .setMessage(mContext.getString(R.string.dialog_delete_frage, sd.getName()))
                .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                    // Zusatzfeld löschen
                    mArbeitsplatz.getDefaultSchichten().delete(index);
                    mNfcTagViewAdapter.notifyItemRemoved(index);
                    mNfcTagViewAdapter.notifyItemChanged(0);
                })
                .setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                    // Do nothing.
                }).show();
    }*/

    @Override
    public void onExpand(int position) {
        mNfcTagListeView.smoothScrollToPosition(position + 1);
    }

    @Override
    public void onNfcTagOpenNamePicker() {
        final InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        NfcBekannterTag openNfcTag = mNfcTagViewAdapter.getOpenItem();

        if (openNfcTag != null) {
            final EditText input = new EditText(mContext);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setLines(1);
            input.setText(openNfcTag.getName());
            input.setSelection(input.getText().length());
            input.setFocusableInTouchMode(true);
            input.requestFocus();
            input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }

            //Längenbegrenzung des Inputstrings
            InputFilter[] fa = new InputFilter[1];
            fa[0] = new InputFilter.LengthFilter(ISettings.LAENGE_NAME);
            input.setFilters(fa);

            new AlertDialog.Builder(mContext)
                    .setTitle(mContext.getString(R.string.bezeichnung))
                    .setView(input)
                    .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                        openNfcTag.setName(input.getText().toString());
                        mNfcTagViewAdapter.notifyItemChanged(mNfcTagViewAdapter.getOpenPosition());
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                        }
                    }).setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                        // Abbruchknopf gedrückt
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                        }
                    }).show();
        }
    }
}

