/* Copyright 2014-2023 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.einsatzort;

import android.content.ContentValues;

import askanimus.arbeitszeiterfassung2.datenbank.DatenbankHelper;
import askanimus.arbeitszeiterfassung2.setup.ASettings;
import askanimus.arbeitszeiterfassung2.setup.ISettings;

/**
 * @author askanimus@gmail.com on 13.12.14.
 */
public class Einsatzort /*implements Comparable<einsatzort>*/ {
    private long Id = -1;
    private long JobId;
    private String Name;
    private int Status = ISettings.STATUS_AKTIV;
    private int anzahl_verwendet;
    private long zueletzt_verwendet;

    public Einsatzort(long id, long arbeitsplatzID, String name, int status, int verwendet, long zuletzt) {
        Id = id;
        JobId = arbeitsplatzID;
        Name = name;
        Status = status;
        anzahl_verwendet = verwendet;
        zueletzt_verwendet = zuletzt;
        if(id < 0)
            save();
    }

    public long getId(){
        return Id;
    }

    public String getName(){

        return Name!= null? Name : "";
    }

    int getStatus(){
        return Status;
    }

    int getVerwendung() {
        return anzahl_verwendet;
    }

    long getzuletzt_Verwendet(){
        return zueletzt_verwendet;
    }

    protected String setName(String name){
        Name = name;
        save();
        return Name;
    }

    private void setStatus(int s){
        if(Status != s) {
            Status = s;
            save();
        }
    }

    void Klone(long ArbeitsplatzID){
        JobId = ArbeitsplatzID;
        Id = -1;
        save();
    }

    public void add_Verwendung(boolean speichern, long datum){
        anzahl_verwendet++;
        //Datum d = new Datum(new Date().getTime(), ASettings.aktJob.getWochenbeginn());
        //d.setTag(1);
        zueletzt_verwendet = datum;//d.getTimeInMillis();
        if(speichern)
            save();
    }

    public void sub_Verwendung(){
        if(anzahl_verwendet > 0) {
            anzahl_verwendet--;
            save();
        }
    }

    public void save(){
        // SQLiteDatabase mDatenbank = ASettings.stundenDBHelper.getWritableDatabase();
        ContentValues werte = new ContentValues();

        werte.put(DatenbankHelper.DB_F_NAME, Name);
        werte.put(DatenbankHelper.DB_F_STATUS, Status);
        werte.put(DatenbankHelper.DB_F_JOB, JobId);
        werte.put(DatenbankHelper.DB_F_ANZAHL_VERWENDET, anzahl_verwendet);
        werte.put(DatenbankHelper.DB_F_ZULETZT_VERWENDET, zueletzt_verwendet);

        if(Id <= -1)
            Id = ASettings.mDatenbank.insert(DatenbankHelper.DB_T_EORT, null, werte);
        else
            ASettings.mDatenbank.update(DatenbankHelper.DB_T_EORT,werte, DatenbankHelper.DB_F_ID+"=?", new String[] {Long.toString(Id)});

        // ASettings.mDatenbank.close();
    }

    /* Zweistufiges Löschen von Einsatzorten */
    void delete() {
        if (anzahl_verwendet <= 0) {
            // SQLiteDatabase mDatenbank = ASettings.stundenDBHelper.getWritableDatabase();
            ASettings.mDatenbank.delete(
                    DatenbankHelper.DB_T_EORT,
                    DatenbankHelper.DB_F_ID + "=?",
                    new String[]{Long.toString(Id)});
            setStatus(ISettings.STATUS_GELOESCHT);
            // ASettings.mDatenbank.close();
        } else {
            if (Status == ISettings.STATUS_INAKTIV)
                setStatus(ISettings.STATUS_GELOESCHT);
            else
                setStatus(ISettings.STATUS_INAKTIV);
        }
    }

    void undelete(){
        setStatus(ISettings.STATUS_AKTIV);
    }
   /* @Override
    public int compareTo(@NonNull einsatzort o) {
        if (zueletzt_verwendet < o.zueletzt_verwendet) {
            return 1;
        } else if(zueletzt_verwendet > o.zueletzt_verwendet) {
            return -1;
        } else  if(anzahl_verwendet < o.anzahl_verwendet) {
            return 1;
        } else if (anzahl_verwendet > o.anzahl_verwendet) {
            return -1;
        } else{
            return Name.compareTo(o.Name);
        }
    }*/
}
