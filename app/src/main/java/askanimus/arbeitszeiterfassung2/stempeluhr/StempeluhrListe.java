/*
 * Copyright (c) 2014 - 2024 askanimus@gmail.com
 *
 * This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

package askanimus.arbeitszeiterfassung2.stempeluhr;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASettings;
import askanimus.arbeitszeiterfassung2.setup.ISettings;
import askanimus.arbeitszeiterfassung2.widget.WidgetStatus;

public class StempeluhrListe extends ArrayList<StempeluhrStatus> implements ISettings {

    public StempeluhrListe(int[] widgetIds){
        super();
        ArrayList<Long> widgetliste = new ArrayList<>();
        for (int widgetID : widgetIds) {
            widgetliste.add(ASettings.mPreferenzen.getLong(WidgetStatus.KEY_JOB + widgetID, 0));
        }

        for (Arbeitsplatz job : ASettings.jobListe.getListe()) {
            StempeluhrStatus status;
            if(!widgetliste.contains(job.getId())) {
                status = new StempeluhrStatus(job, size());
                if (!job.isEndeAufzeichnung(ASettings.aktDatum)) {
                    add(status);
                } else {
                    // die Aufzeicnungen für diesen Job wurden beendet
                    // es wird keine Stempeluhr mehr benötigt
                    if (status.getStatus() != WidgetStatus.WIDGET_STATUS_NEU) {
                        // läuft sie noch, dann anzeigen und nach dem beenden löschen
                        add(status);
                    } else {
                        //Stempeluhr löschen wenn sie nicht läuft
                        status.delete(ASettings.mPreferenzen);
                    }
                }
            }
        }
    }

}
