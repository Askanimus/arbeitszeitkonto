/*
 * Copyright (c) 2014 - 2024 askanimus@gmail.com
 *
 * This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

package askanimus.arbeitszeiterfassung2.stempeluhr;

import android.app.Dialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import askanimus.arbeitszeiterfassung2.MinutenInterpretationDialog;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.Arbeitsschicht;
import askanimus.arbeitszeiterfassung2.setup.ASettings;
import askanimus.arbeitszeiterfassung2.setup.ISettings;
import askanimus.arbeitszeiterfassung2.widget.Widget;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.Bereichsfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertAuswahlListe;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzwertAuswahlDialog;
import askanimus.betterpickers.numberpicker.NumberPickerBuilder;
import askanimus.betterpickers.numberpicker.NumberPickerDialogFragment;
import askanimus.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import askanimus.betterpickers.timepicker.TimePickerBuilder;
import askanimus.betterpickers.timepicker.TimePickerDialogFragment;

public class StempeluhrFragment
        extends
        Fragment
        implements
        StempeluhrExpandListAdapter.ItemClickListener,
        TimePickerDialogFragment.TimePickerDialogHandler,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2,
        RadialTimePickerDialogFragment.OnTimeSetListener,
        ZusatzwertAuswahlDialog.ZusatzwertAuswahlDialogCallbacks
{
    private Context mContext;
    private RecyclerView viewStempeluhren;
    private StempeluhrExpandListAdapter mStempeluhrListAdapter;
    private StempeluhrStatus mEditStempeluhrStatus = null;
    private IZusatzfeld mEditZusatzfeld = null;

    // das Item der Auswahlliste, welches neu angelegt wurde
    private ZusatzWertAuswahlListe.zusatzWertAuswahlItem mAuswahlItemNeu;
    private ZusatzwertAuswahlDialog auswahlDialog;
    final Handler refreshHandler = new Handler();
    Runnable runnable = null;

    public static StempeluhrFragment newInstance(String stempelAktion, long stempeluhrId) {
        Bundle args = new Bundle();
        args.putString(StempeluhrStatus.KEY_STEMPELUHR_AKTION, stempelAktion);
        args.putLong(StempeluhrStatus.KEY_STEMPELUHR_ID, stempeluhrId);
        StempeluhrFragment fragment = new StempeluhrFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        //super.onCreateView(inflater, container, savedInstanceState);
        mContext = getContext();
        View view = inflater.inflate(R.layout.fragment_stempeluhr, container, false);
        viewStempeluhren = view.findViewById(R.id.SU_liste);
        mStempeluhrListAdapter = new StempeluhrExpandListAdapter();
        LinearLayoutManager gLayoutManager = new LinearLayoutManager(mContext);
        gLayoutManager.setSmoothScrollbarEnabled(true);
        gLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        viewStempeluhren.setHasFixedSize(true);
        viewStempeluhren.setLayoutManager(gLayoutManager);
        viewStempeluhren.setAdapter(mStempeluhrListAdapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ASettings.init(mContext, this::resume);
    }

    void resume() {
        // Standartelemente finden
        View mInhalt = getView();
        if (mInhalt != null) {
            // Die aktuellen Widgets durchsuchen, ob eines des Arbeitsplatzes des Tags läuft
            AppWidgetManager wm = AppWidgetManager.getInstance(mContext);
            ComponentName cn = new ComponentName(mContext.getPackageName(), Widget.class.getName());
            mStempeluhrListAdapter.setUp(mContext, new StempeluhrListe(wm.getAppWidgetIds(cn)), this);

            Bundle args = getArguments();
            if (args != null) {
                long stempeluhrId = args.getLong(StempeluhrStatus.KEY_STEMPELUHR_ID, 0);
                if (stempeluhrId > 0) {
                    String aktion = args.getString(StempeluhrStatus.KEY_STEMPELUHR_AKTION, "");
                    if (!aktion.isEmpty()) {
                        mStempeluhrListAdapter.aktion(stempeluhrId, aktion);
                    }
                }

            }
        }

        if (runnable == null) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    // folgender Update nach einer Minute
                    for(int i = 0; i<mStempeluhrListAdapter.getItemCount();i++) {
                        mStempeluhrListAdapter.updateStempelUhr(i, -1);
                    }
                    refreshHandler.postDelayed(this, 60000);
                }
            };
            // erster Update zur nächsten vollen Minute
            long sysTime = 60000 - System.currentTimeMillis() % 60000;
            refreshHandler.postDelayed(runnable, sysTime);
        }
    }

    @Override
    public void onPause() {
        refreshHandler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    public void onExpand(int position) {
        viewStempeluhren.smoothScrollToPosition(position + 1);
    }

    @Override
    public void onStempeluhrOpenPicker(StempeluhrStatus status, int wert) {
        final FragmentManager fManager;
        try {
            fManager = getParentFragmentManager();
            switch (wert) {
                case Arbeitsschicht.WERT_VON:
                    mEditStempeluhrStatus = status;
                    Uhrzeit mZeitVon = new Uhrzeit(mEditStempeluhrStatus.getVon());
                    /*
                     * Wenn die Zeit am aktuellen Tag, in der Nähe (+- 2 Stunden)
                     * der eingetragenen Zeit verändert werden soll,
                     * wird die aktuelle Zeit im Dialog eingestellt
                     */
                        Calendar v_cal = Calendar.getInstance();
                        int v_aktuell = v_cal.get(Calendar.HOUR_OF_DAY);
                        int v_stempel = mZeitVon.getStunden();
                        if (v_aktuell >= (v_stempel - 2) && v_aktuell <= (v_stempel + 2)) {
                            mZeitVon.set(v_aktuell, v_cal.get(Calendar.MINUTE));
                        }
                    RadialTimePickerDialogFragment vonPickerDialog =
                            new RadialTimePickerDialogFragment()
                                    .setOnTimeSetListener(this)
                                    .setStartTime(
                                            mZeitVon.getStunden(),
                                            mZeitVon.getMinuten());
                    if (ASettings.isThemaDunkel)
                        vonPickerDialog.setThemeDark();
                    else
                        vonPickerDialog.setThemeLight();
                    vonPickerDialog.show(fManager, String.valueOf(wert));
                    break;
                case Arbeitsschicht.WERT_BIS:
                    mEditStempeluhrStatus = status;
                    Uhrzeit mZeitBis = new Uhrzeit(mEditStempeluhrStatus.getBis());
                    /*
                     * Wenn die Zeit am aktuellen Tag, in der Nähe (+- 2 Stunden)
                     * der eingetragenen Zeit verändert werden soll,
                     * wird die aktuelle Zeit im Dialog eingestellt
                     */
                        Calendar b_cal = Calendar.getInstance();
                        int b_aktuell = b_cal.get(Calendar.HOUR_OF_DAY);
                        int b_stempel = mZeitBis.getStunden();
                        if (b_aktuell >= (b_stempel - 2) && b_aktuell <= (b_stempel + 2)) {
                            mZeitBis.set(b_aktuell, b_cal.get(Calendar.MINUTE));
                        }
                    RadialTimePickerDialogFragment bisPickerDialog =
                            new RadialTimePickerDialogFragment()
                                    .setOnTimeSetListener(this)
                                    .setStartTime(
                                            mZeitBis.getStunden(),
                                            mZeitBis.getMinuten());
                    if (ASettings.isThemaDunkel)
                        bisPickerDialog.setThemeDark();
                    else
                        bisPickerDialog.setThemeLight();
                    bisPickerDialog.show(fManager, String.valueOf(wert));
                    break;
                case Arbeitsschicht.WERT_PAUSE:
                    mEditStempeluhrStatus = status;
                    if (mEditStempeluhrStatus.getArbeitsplatz().isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)) {
                        NumberPickerBuilder pausePicker = new NumberPickerBuilder()
                                .setFragmentManager(fManager)
                                .setStyleResId(ASettings.themePicker)
                                .setMinNumber(BigDecimal.valueOf(0))
                                .setLabelText(getString(R.string.k_stunde))
                                .setPlusMinusVisibility(View.INVISIBLE)
                                .setDecimalVisibility(View.VISIBLE)
                                .setReference(wert)
                                .setTargetFragment(this);
                        pausePicker.show();
                    } else {
                        TimePickerBuilder tpb = new TimePickerBuilder()
                                .setFragmentManager(fManager)
                                .setTargetFragment(this)
                                .setReference(wert)
                                .setStyleResId(ASettings.themePicker)
                                .addTimePickerDialogHandler(this);
                        tpb.show();
                    }
                    break;
            }
        } catch (IllegalStateException ignore) {
        }
    }

    @Override
    public void onZusatzfeldOpenPicker(StempeluhrStatus status, IZusatzfeld feld, int wert) {
        final FragmentManager fManager;
        try {
            fManager = getParentFragmentManager();
            switch (wert) {
                case Arbeitsschicht.WERT_ZUSATZ_PROZENT:
                case Arbeitsschicht.WERT_ZUSATZ_ZAHL:
                case Arbeitsschicht.WERT_ZUSATZ_ZAHL_VON:
                case Arbeitsschicht.WERT_ZUSATZ_ZAHL_BIS:
                    mEditZusatzfeld = feld;
                    mEditStempeluhrStatus = status;
                    NumberPickerBuilder nPicker = new NumberPickerBuilder()
                            .setFragmentManager(fManager)
                            .setStyleResId(ASettings.themePicker)
                            .setMinNumber(BigDecimal.valueOf(0))
                            .setLabelText(feld.getEinheit())
                            .setPlusMinusVisibility(View.INVISIBLE)
                            .setDecimalVisibility(View.VISIBLE)
                            .setReference(wert)
                            .setTargetFragment(this);
                    nPicker.show();
                    break;
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT:
                    mEditZusatzfeld = feld;
                    mEditStempeluhrStatus = status;
                    if (mEditStempeluhrStatus.getArbeitsplatz().isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)) {
                        NumberPickerBuilder zPicker = new NumberPickerBuilder()
                                .setFragmentManager(fManager)
                                .setStyleResId(ASettings.themePicker)
                                .setMinNumber(BigDecimal.valueOf(0))
                                .setMaxNumber(BigDecimal.valueOf(24))
                                .setLabelText(feld.getEinheit())
                                .setPlusMinusVisibility(View.INVISIBLE)
                                .setDecimalVisibility(View.VISIBLE)
                                .setReference(wert)
                                .setTargetFragment(this);
                        zPicker.show();
                    } else {
                        TimePickerBuilder zPicker = new TimePickerBuilder()
                                .setFragmentManager(fManager)
                                .setTargetFragment(this)
                                .setReference(wert)
                                .setStyleResId(ASettings.themePicker)
                                .addTimePickerDialogHandler(this);
                        zPicker.show();
                    }
                    break;
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON:
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS:
                    int w;
                    if(wert == Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON){
                        w = (int) ((Bereichsfeld)feld).getVon().getWert();
                    } else {
                        w = (int) ((Bereichsfeld)feld).getBis().getWert();
                    }
                    mEditZusatzfeld = feld;
                    mEditStempeluhrStatus = status;
                    Uhrzeit mZeit = new Uhrzeit(w);
                    RadialTimePickerDialogFragment zusatzPickerDialog =
                            new RadialTimePickerDialogFragment()
                                    .setOnTimeSetListener(this)
                                    .setStartTime(
                                            mZeit.getStunden(),
                                            mZeit.getMinuten());
                    if (ASettings.isThemaDunkel)
                        zusatzPickerDialog.setThemeDark();
                    else
                        zusatzPickerDialog.setThemeLight();
                    zusatzPickerDialog.show(fManager, String.valueOf(wert));
                    break;
                case Arbeitsschicht.WERT_ZUSATZ_AUSWAHL:
                    mEditZusatzfeld = feld;
                    mEditStempeluhrStatus = status;
                    auswahlDialog = new ZusatzwertAuswahlDialog(mContext, this, feld, mEditStempeluhrStatus.getArbeitsplatz());
                    auswahlDialog.open();
                    break;
            }
        } catch (IllegalStateException ignore) {
        }

    }


    @Override
    public void onDialogNumberSet(final int reference, final BigInteger number, final double decimal, boolean isNegative, BigDecimal fullNumber) {
        MinutenInterpretationDialog.MinutenInterpretationDialogListener mListener = null;

        switch (reference) {
            case Arbeitsschicht.WERT_PAUSE:
                if (mEditStempeluhrStatus != null) {
                    mListener = z -> {
                        mEditStempeluhrStatus.setPauseManuell(z.getAlsMinuten());
                        mStempeluhrListAdapter.updateStempelUhr(mEditStempeluhrStatus.getListPosition(), -1);
                        //mStempeluhrListAdapter.notifyItemChanged(mEditStempeluhrStatus.getListPosition());
                        mEditStempeluhrStatus = null;
                    };
                }
                break;
            case Arbeitsschicht.WERT_ZUSATZ_ZEIT:
            case Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON:
            case Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS:
                if (mEditZusatzfeld != null) {
                    final IZusatzfeld feld;
                    if(reference == Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON){
                        feld = ((Bereichsfeld)mEditZusatzfeld).getVon();
                    } else if(reference == Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS){
                        feld = ((Bereichsfeld)mEditZusatzfeld).getBis();
                    } else {
                        feld = mEditZusatzfeld;
                    }
                    mListener = z -> {
                        feld.setWert(z.getAlsMinuten());
                        mEditStempeluhrStatus.save(ASettings.mPreferenzen);
                        // das neu angelegte Auswahlitem mit dem neuen Wert versehen und speichern
                        if (mAuswahlItemNeu != null) {
                            mAuswahlItemNeu.setWert(mEditZusatzfeld);
                            mAuswahlItemNeu = null;
                        }
                        mStempeluhrListAdapter.updateStempelUhr(mEditStempeluhrStatus.getListPosition(), mEditZusatzfeld.getPosition());
                        //mStempeluhrListAdapter.notifyItemChanged(mEditStempeluhrStatus.getListPosition());
                        mEditZusatzfeld = null;
                    };
                }
                break;
            case Arbeitsschicht.WERT_ZUSATZ_PROZENT:
            case Arbeitsschicht.WERT_ZUSATZ_ZAHL:
            case Arbeitsschicht.WERT_ZUSATZ_ZAHL_VON:
            case Arbeitsschicht.WERT_ZUSATZ_ZAHL_BIS:
                if (mEditZusatzfeld != null) {
                    IZusatzfeld feld = mEditZusatzfeld;
                    if(reference == Arbeitsschicht.WERT_ZUSATZ_ZAHL_VON){
                        feld = ((Bereichsfeld)mEditZusatzfeld).getVon();
                    } else if(reference == Arbeitsschicht.WERT_ZUSATZ_ZAHL_BIS){
                        feld = ((Bereichsfeld)mEditZusatzfeld).getBis();
                    }
                    feld.setWert(fullNumber.floatValue());
                    mEditStempeluhrStatus.save(ASettings.mPreferenzen);
                    // das neu angelegte Auswahlitem mit dem neuen Wert versehen und speichern
                    if(mAuswahlItemNeu != null) {
                        mAuswahlItemNeu.setWert(mEditZusatzfeld);
                        mAuswahlItemNeu = null;
                    }
                    mStempeluhrListAdapter.updateStempelUhr(mEditStempeluhrStatus.getListPosition(), mEditZusatzfeld.getPosition());
                    // mStempeluhrListAdapter.notifyItemChanged(mEditStempeluhrStatus.getListPosition());
                    mEditZusatzfeld = null;
                }
                break;
        }

        if (mListener != null) {
            new MinutenInterpretationDialog(
                    mContext,
                    mEditStempeluhrStatus.getArbeitsplatz().isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL),
                    /*number,*/
                    decimal,
                    fullNumber,
                    mListener
            );
        }
    }

    @Override
    public void onDialogTimeSet(int reference, int hourOfDay, int minute) {
        switch (reference) {
            case Arbeitsschicht.WERT_PAUSE:
                if (mEditStempeluhrStatus != null) {
                    mEditStempeluhrStatus.setPauseManuell(Uhrzeit.makeMinuten(hourOfDay, minute));
                    mStempeluhrListAdapter.updateStempelUhr(mEditStempeluhrStatus.getListPosition(), -1);
                    //mStempeluhrListAdapter.notifyItemChanged(mEditStempeluhrStatus.getListPosition());
                    mEditStempeluhrStatus = null;
                }
                break;
            case Arbeitsschicht.WERT_ZUSATZ_ZEIT:
            case Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON:
            case Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS:
                if (mEditZusatzfeld != null) {
                    IZusatzfeld feld = mEditZusatzfeld;
                    if (reference == Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON) {
                        feld = ((Bereichsfeld) mEditZusatzfeld).getVon();
                    } else if (reference == Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS) {
                        feld = ((Bereichsfeld) mEditZusatzfeld).getBis();
                    }
                    feld.setWert(Uhrzeit.makeMinuten(hourOfDay, minute));
                    mEditStempeluhrStatus.save(ASettings.mPreferenzen);
                    // das neu angelegte Auswahlitem mit dem neuen Wert versehen und speichern
                    if (mAuswahlItemNeu != null) {
                        mAuswahlItemNeu.setWert(mEditZusatzfeld);
                        mAuswahlItemNeu = null;
                    }
                    mStempeluhrListAdapter.updateStempelUhr(mEditStempeluhrStatus.getListPosition(), mEditZusatzfeld.getPosition());
                    //mStempeluhrListAdapter.notifyItemChanged(mEditStempeluhrStatus.getListPosition());
                    mEditZusatzfeld = null;
                }
        }
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        String dTag = dialog.getTag();
        if (dTag != null) {
            int reference = Integer.parseInt(dTag);
            switch (reference) {
                case Arbeitsschicht.WERT_VON:
                    if (mEditStempeluhrStatus!= null) {
                        mEditStempeluhrStatus.setVon(Uhrzeit.makeMinuten(hourOfDay, minute));
                        mStempeluhrListAdapter.updateStempelUhr(mEditStempeluhrStatus.getListPosition(), -1);
                        //mStempeluhrListAdapter.notifyItemChanged(mEditStempeluhrStatus.getListPosition());
                        mEditStempeluhrStatus = null;
                    }
                    break;
                case Arbeitsschicht.WERT_BIS:
                    if (mEditStempeluhrStatus!= null) {
                        mStempeluhrListAdapter.ausstempeln(
                                mEditStempeluhrStatus.getArbeitsplatz(),
                                mEditStempeluhrStatus.runden(
                                        Uhrzeit.makeMinuten(hourOfDay, minute),
                                        mEditStempeluhrStatus.mRundenBis,
                                        mEditStempeluhrStatus.mRundenPause_minuten
                                ).getAlsMinuten()
                        );
                        mEditStempeluhrStatus.stop();
                        mStempeluhrListAdapter.updateStempelUhr(mEditStempeluhrStatus.getListPosition(), -1);
                        //mStempeluhrListAdapter.notifyItemChanged(mEditStempeluhrStatus.getListPosition());
                        mEditStempeluhrStatus = null;
                    }
                    break;
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT:
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON:
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS:
                    if (mEditZusatzfeld != null) {
                        IZusatzfeld feld = mEditZusatzfeld;
                        if (reference == Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON) {
                            feld = ((Bereichsfeld) mEditZusatzfeld).getVon();
                        } else if (reference == Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS) {
                            feld = ((Bereichsfeld) mEditZusatzfeld).getBis();
                        }
                        feld.setWert(Uhrzeit.makeMinuten(hourOfDay, minute));
                        mEditStempeluhrStatus.save(ASettings.mPreferenzen);
                        // das neu angelegte Auswahlitem mit dem neuen Wert versehen und speichern
                        if (mAuswahlItemNeu != null) {
                            mAuswahlItemNeu.setWert(mEditZusatzfeld);
                            mAuswahlItemNeu = null;
                        }
                        mStempeluhrListAdapter.updateStempelUhr(mEditStempeluhrStatus.getListPosition(), mEditZusatzfeld.getPosition());
                        //mStempeluhrListAdapter.notifyItemChanged(mEditStempeluhrStatus.getListPosition());
                        mEditZusatzfeld = null;
                    }
            }
        }
    }

    @Override
    public void onZusatzwertSet(IZusatzfeld wert) {
        if (mEditZusatzfeld != null) {
            mEditZusatzfeld.set(wert);
            mEditStempeluhrStatus.save(ASettings.mPreferenzen);
            mStempeluhrListAdapter.updateStempelUhr(mEditStempeluhrStatus.getListPosition(), mEditZusatzfeld.getPosition());
            //mStempeluhrListAdapter.notifyItemChanged(mEditStempeluhrStatus.getListPosition());
            mEditZusatzfeld = null;
        }
    }

    @Override
    public void onZusatzwertAdd(IZusatzfeld feld, ZusatzWertAuswahlListe.zusatzWertAuswahlItem eintragNeu) {
        switch (feld.getDatenTyp()) {
            case IZusatzfeld.TYP_AUSWAHL_ZAHL:
                mAuswahlItemNeu = eintragNeu;
                onZusatzfeldOpenPicker(mEditStempeluhrStatus, feld, Arbeitsschicht.WERT_ZUSATZ_ZAHL);
                break;
            case IZusatzfeld.TYP_AUSWAHL_PROZENT:
                mAuswahlItemNeu = eintragNeu;
                onZusatzfeldOpenPicker(mEditStempeluhrStatus, feld, Arbeitsschicht.WERT_ZUSATZ_PROZENT);
                break;
            case IZusatzfeld.TYP_AUSWAHL_ZEIT:
                mAuswahlItemNeu = eintragNeu;
                onZusatzfeldOpenPicker(mEditStempeluhrStatus, feld, Arbeitsschicht.WERT_ZUSATZ_ZEIT);
                break;
            default:
                final InputMethodManager imm =
                        (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                final EditText mInput = new EditText(mContext);
                mInput.setText("");
                mInput.setFocusableInTouchMode(true);
                mInput.setInputType(
                        InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                                | InputType.TYPE_CLASS_TEXT
                                | InputType.TYPE_TEXT_FLAG_MULTI_LINE
                );

                //Längenbegrenzung des Inputstrings
                InputFilter[] fa = new InputFilter[1];
                fa[0] = new InputFilter.LengthFilter(ISettings.LAENGE_NOTIZ);
                mInput.setFilters(fa);

                new AlertDialog.Builder(mContext)
                        .setTitle(feld.getName())
                        .setView(mInput)
                        .setPositiveButton(
                                mContext.getString(android.R.string.ok),
                                (dialog, whichButton) -> {
                                    if (whichButton == Dialog.BUTTON_POSITIVE) {
                                        // ein neues leeres Zusatzfeld anlegen
                                        IZusatzfeld feldNeu = feld.getDefinition().makeNewZusatzfeld();
                                        // den neuen Wert eintragen
                                        feldNeu.setWert(mInput.getText().toString());
                                        // das neu angelegte Auswahlitem mit dem neuen Wert versehen und speichern
                                        eintragNeu.setWert(feldNeu);
                                        // die Auswahlliste erweitern
                                        auswahlDialog.addWert(eintragNeu);
                                        if (imm != null) {
                                            imm.hideSoftInputFromWindow(mInput.getWindowToken(), 0);
                                        }
                                    }
                                }).setNegativeButton(
                                mContext.getString(android.R.string.cancel),
                                (dialog, whichButton) -> {
                                    // Abbruchknopf gedrückt
                                    if (imm != null) {
                                        imm.hideSoftInputFromWindow(mInput.getWindowToken(), 0);
                                    }
                                }).show();

                mInput.requestFocus();
                if (imm != null) {
                    imm.toggleSoftInputFromWindow(
                            mInput.getWindowToken(),
                            InputMethodManager.SHOW_FORCED,
                            0
                    );
                }
        }
    }
}
