/* Copyright 2014-2023 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.abwesenheiten;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASettings;

/**
 * Created by ascanimus@gmail.com on 08.12.17.
 */

public class AbwesenheitListadapter extends BaseAdapter {
    private final Context mContext;
    private final Arbeitsplatz mArbeitsplatz;
    private final int mKategorie;

    private int selectAbwesenheit;

    ArrayList<Abwesenheit> mListeAbwesenheiten;

    /**
     * Abwesenheitsliste als Adapter für Asuwahllisten
     * @param context der Context der App
     * @param arbeitsplatz der Arbeitsplatz von dem die Abwesenheiten stammen
     * @param kategorie Filter um die Liste auf eine Kategorie zu beschränken; -1 = alle Abwesenheiten
     */
    public AbwesenheitListadapter(Context context, Arbeitsplatz arbeitsplatz, int kategorie) {
        mContext = context;
        mArbeitsplatz = arbeitsplatz;
        mKategorie = kategorie;

        if(kategorie >= 0) {
            mListeAbwesenheiten = new ArrayList<>();
            for (Abwesenheit abw : mArbeitsplatz.getAbwesenheiten().getListeAktive()) {
                if (abw.getKategorie() == kategorie){
                    mListeAbwesenheiten.add(abw);
                }
            }
        } else {
            mListeAbwesenheiten =  mArbeitsplatz.getAbwesenheiten().getListeAktive();
        }
    }

    @Override
    public int getCount() {
        return mListeAbwesenheiten.size();
    }

    @Override
    public Abwesenheit getItem(int position) {
        return mListeAbwesenheiten.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getID();
    }

    private int getItemPosition(long id){
        int i = 0;
        for (Abwesenheit abw : mListeAbwesenheiten) {
            if(abw.getID() == id){
                return i;
            }
            i ++;
        }
        return -1;
    }

    public int setSelectAbwesenheit(long selectAbwesenheit) {
        int i = getItemPosition(selectAbwesenheit);
        if( i >= 0) {
            this.selectAbwesenheit = i;
        }
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            TextView row = (TextView) inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            if(selectAbwesenheit == position){
                row.setTypeface(null, Typeface.BOLD);;
            }
            row.setText(getItem(position).getName());
            return (row);
        } else
            return null;
    }
}
