/*
 * Copyright (c) 2014 - 2024 askanimus@gmail.com
 *
 * This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

package askanimus.arbeitszeiterfassung2.nfc;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import askanimus.arbeitszeiterfassung2.datenbank.DatenbankHelper;
import askanimus.arbeitszeiterfassung2.setup.ASettings;

public class NfcBekannterTag implements INfc {
    private long dbID = -1;
    private long tagID;
    private long arbeitsplatz;
    private int aktion;
    String name;
    public NfcBekannterTag(long id, long job){
        this.tagID = id;
        this.arbeitsplatz = job;
        this.aktion = TAG_AKTION_KEINE;
        this.name = String.valueOf(tagID);
        speichern();
    }

    public NfcBekannterTag(Cursor tagDaten){
        dbID = tagDaten.getLong(tagDaten.getColumnIndex(DatenbankHelper.DB_F_ID));
        tagID = tagDaten.getLong(tagDaten.getColumnIndex(DatenbankHelper.DB_F_NFC_TAGID));
        arbeitsplatz = tagDaten.getLong(tagDaten.getColumnIndex(DatenbankHelper.DB_F_JOB));
        name = tagDaten.getString(tagDaten.getColumnIndex(DatenbankHelper.DB_F_NAME));
        aktion = tagDaten.getInt(tagDaten.getColumnIndex(DatenbankHelper.DB_F_NFC_AKTION));
    }


    public long getTagID(){
        return tagID;
    }

    public long getArbeitsplatzID(){
        return arbeitsplatz;
    }

    public int getAktion(){
        return aktion;
    }
    public String getName() {
        return name;
    }


    public void setName(String name){
        if(!this.name.equals(name)) {
            this.name = name == null ? String.valueOf(tagID) : name;
            speichern();
        }
    }
    public void setAktion(int aktion){
        if(this.aktion != aktion) {
            this.aktion = aktion;
            speichern();
        }
    }

    private void speichern(){
        // SQLiteDatabase mDatenbank = ASettings.stundenDBHelper.getWritableDatabase();
        ContentValues werte = new ContentValues();

        werte.put(DatenbankHelper.DB_F_NFC_TAGID, tagID);
        werte.put(DatenbankHelper.DB_F_JOB, arbeitsplatz);
        werte.put(DatenbankHelper.DB_F_NAME, name);
        werte.put(DatenbankHelper.DB_F_NFC_AKTION, aktion);

        if(dbID < 0){
            dbID = ASettings.mDatenbank.insert(DatenbankHelper.DB_T_NFC_TAG, null, werte);
        } else {
            ASettings.mDatenbank.update(
                    DatenbankHelper.DB_T_NFC_TAG, werte,
                    DatenbankHelper.DB_F_ID + "=?",
                    new String[]{Long.toString(dbID)}
            );
        }

        // mDatenbank.close();
    }

    public void loeschen(){
        // SQLiteDatabase mDatenbank = ASettings.stundenDBHelper.getWritableDatabase();
        ASettings.mDatenbank.delete(
                DatenbankHelper.DB_T_NFC_TAG,
                    DatenbankHelper.DB_F_ID + "=?",
                    new String[]{Long.toString(dbID)}
        );
        // mDatenbank.close();
    }
}
