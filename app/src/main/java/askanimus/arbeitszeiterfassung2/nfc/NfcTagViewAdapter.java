/*
 * Copyright (c) 2014 - 2024 askanimus@gmail.com
 *
 * This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.nfc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;

public class NfcTagViewAdapter
        extends RecyclerView.Adapter<NfcTagViewAdapter.ViewHolder> {
    private Context mContext;
    private Arbeitsplatz mArbeitsplatz;
    private NfcTagListe mNfcTagListe;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private int openPosition = -1;
    private ViewHolder openHolder = null;

    public NfcTagViewAdapter setUp(
            Context context,
            Arbeitsplatz arbeitsplatz,
            ItemClickListener clickListener,
            NfcTagListe tags
    ){
        mContext = context;
        mArbeitsplatz = arbeitsplatz;
        mInflater = LayoutInflater.from(context);
        mNfcTagListe = tags;
        mClickListener = clickListener;

        return this;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_nfc_tag, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NfcTagViewAdapter.ViewHolder holder, int position) {
        if (position < getItemCount()) {

            NfcBekannterTag mNfcTag = getItem(position);

            holder.tTitel.setText(mNfcTag.getName());

            // die Werte des Tags anzeigen oder verbergen
            if (position == openPosition) {
                openHolder = holder;
                holder.mErweitert.setVisibility(View.VISIBLE);
                holder.iArrow.setImageResource(R.drawable.arrow_up);
                updateView(holder);
            }
        }
    }

    private void updateView(ViewHolder holder){
        NfcBekannterTag mNfcTag = getItem(holder.getAdapterPosition());

        if(mNfcTag != null) {
            // Werte eintragen
            holder.tName.setText(mNfcTag.getName());

            // Icon
            if(mNfcTag.getAktion() == INfc.TAG_AKTION_START_STOP){
                holder.iAktion.setImageResource(R.drawable.start_stop);
                holder.hAktion.setText(String.format(
                        mContext.getString(R.string.nfc_hint_startstop),
                        mArbeitsplatz.getName()));
            } else if(mNfcTag.getAktion() == INfc.TAG_AKTION_PAUSE){
                holder.iAktion.setImageResource(R.drawable.pause);
                holder.hAktion.setText(String.format(
                        mContext.getString(R.string.nfc_hint_pause),
                        mArbeitsplatz.getName()));
            } else {
                holder.iAktion.setVisibility(View.INVISIBLE);
                holder.hAktion.setText("");
            }


            holder.sAktion.setSelection(mNfcTag.getAktion());
            holder.sAktion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mNfcTag.setAktion(position);
                    updateView(holder);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //Klickhandler registrieren
            holder.tName.setOnClickListener(v -> mClickListener.onNfcTagOpenNamePicker()
            );

        }
    }

    @Override
    public int getItemCount() {
        if(mNfcTagListe != null) {
            return mNfcTagListe.size();
        }
        return 0;
    }

    NfcBekannterTag getItem(int index) {
        if (index < getItemCount()) {
            return mNfcTagListe.get(index);
        }
        return null;
    }

    public int getOpenPosition(){
        return openPosition;
    }

    public NfcBekannterTag getOpenItem(){
        if(openPosition >= 0 && openPosition < mNfcTagListe.size()){
            return getItem(openPosition);
        }
        return null;
    }

    // öffne NFC Tag
    public void openItem(int position){
        // die zuvor offene Abwesenheit schliessen
        if(openHolder != null){
           openHolder.mErweitert.setVisibility(View.GONE);
        }

        // neue Abwesenheit als geöffnet markieren
        openPosition = Math.min(position, getItemCount() - 1);
        mClickListener.onExpand(openPosition);
    }


    // vom Fragment zu bearbeitender Click
    public interface ItemClickListener {
        /**
         * Aufgerufen wenn sich Werte der Schicht geändert haben
         */
        //void onNfcTagDelete(int index);
        void onExpand(int position);

        // wenn ein Picker geöffnet werden soll
        void onNfcTagOpenNamePicker();
    }

    class ViewHolder
            extends RecyclerView.ViewHolder {
        TextView tTitel;
        ImageView mDelete;
        ImageView iArrow;
        RelativeLayout mErweitert;
        TextView tName;
        Spinner sAktion;
        ImageView iAktion;
        TextView hAktion;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tTitel = itemView.findViewById(R.id.NFC_name_kopf);
            mDelete = itemView.findViewById(R.id.NFC_button_kopf);
            iArrow = itemView.findViewById(R.id.NFC_arrow);
            mErweitert = itemView.findViewById(R.id.NFC_frame_detail);
            tName = itemView.findViewById(R.id.NFC_wert_tagname);
            sAktion = itemView.findViewById(R.id.NFC_spinner_aktion);
            iAktion = itemView.findViewById(R.id.NFC_wert_icon);
            hAktion = itemView.findViewById(R.id.NFC_hint_action);

            RelativeLayout bTitel = itemView.findViewById(R.id.NFC_kopf_frame);
            bTitel.setBackgroundColor(mArbeitsplatz.getFarbe_Tag());
            bTitel.setOnClickListener(v -> expand(getAdapterPosition()));

            if (mClickListener != null) {
                //mDelete.setOnClickListener(v -> mClickListener.onNfcTagDelete(getAdapterPosition()));

                mDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NfcBekannterTag nfcTag = getItem(getAdapterPosition());
                        new AlertDialog.Builder(mContext)
                                .setTitle(mContext.getString(R.string.dialog_delete, nfcTag.getName()))
                                .setMessage(mContext.getString(R.string.dialog_delete_frage, nfcTag.getName()))
                                .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                                    // nfcTag löschen
                                    mNfcTagListe.removeTag(getAdapterPosition());
                                    notifyItemRemoved(getAdapterPosition());
                                    notifyItemChanged(0);
                                })
                                .setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                                    // Do nothing.
                                }).show();
                    }
                });
            }
        }


        public void expand(int position) {
            if (mClickListener != null) {
                // den alten Inhalt schliessen
                if (openHolder != null) {
                    openHolder.mErweitert.setVisibility(View.GONE);
                    openHolder.iArrow.setImageResource(R.drawable.arrow_down);
                }
                // den neuen Inhalt öffnen, wenn der neue nicht der alte ist
                // wenn es der alte ist, dann diesen wierder schließen
                if (openHolder  == this) {
                    openHolder = null;
                    openPosition = -1;
                } else {
                    openPosition = position;
                    openHolder = this;
                    iArrow.setImageResource(R.drawable.arrow_up);

                    updateView(this);

                    // Erweiterung öffnen
                    mErweitert.setVisibility(View.VISIBLE);
                    mClickListener.onExpand(position);
                }
            }
        }

    }
}
