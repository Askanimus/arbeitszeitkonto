/*
 * Copyright (c) 2014 - 2024 askanimus@gmail.com
 *
 * This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

package askanimus.arbeitszeiterfassung2.nfc;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.datenbank.DatenbankHelper;
import askanimus.arbeitszeiterfassung2.setup.ASettings;
import askanimus.arbeitszeiterfassung2.setup.ISettings;

public class NfcTagListe extends ArrayList<NfcBekannterTag> implements ISettings {

    @SuppressLint("Range")
    public NfcTagListe(long arbeitsplatz) {
        super();
        // SQLiteDatabase mDatenbank = ASettings.stundenDBHelper.getReadableDatabase();
        final String SQL_READ_NFCTAGS =
            "select * from "
                    + DatenbankHelper.DB_T_NFC_TAG
                    + " where "
                    + DatenbankHelper.DB_F_JOB
                    + "= ?";

        Cursor resultNfcTags = ASettings.mDatenbank.rawQuery(
                SQL_READ_NFCTAGS,
                new String[]{Long.toString(arbeitsplatz)}
        );

        while (resultNfcTags.moveToNext()) {
            add(new NfcBekannterTag(resultNfcTags));
        }
        resultNfcTags.close();
        // mDatenbank.close();
    }

    @SuppressLint("Range")
    public NfcTagListe() {
        super();
        // SQLiteDatabase mDatenbank = ASettings.stundenDBHelper.getReadableDatabase();
        final String SQL_READ_NFCTAGS =
            "select * from "
                    + DatenbankHelper.DB_T_NFC_TAG;

        Cursor resultNfcTags = ASettings.mDatenbank.rawQuery(
                SQL_READ_NFCTAGS,
                new String[]{}
        );

        while (resultNfcTags.moveToNext()) {
            add(new NfcBekannterTag(resultNfcTags));
        }
        resultNfcTags.close();
        // mDatenbank.close();
    }

    public int addTag(long tagId, long arbeitsplatz){
        if(tagId > 0) {
            int position = getTagIndex(tagId);
            if (position < 0) {
                add(new NfcBekannterTag(tagId, arbeitsplatz));
                position = size() - 1;
            }
            return position;
        }
        return -1;
    }

    public void removeTag(int position){
        get(position).loeschen();
        remove(position);
    }

    public int getTagIndex(long tagID) {
        int index = 0;
        for (NfcBekannterTag tag : this) {
            if (tag.getTagID() == tagID) {
                return index;
            }
            index++;
        }
        return -1;
    }
}
