/* Copyright 2014-2023 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsplatz;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import java.util.Objects;

import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.setup.ASettings;
import askanimus.arbeitszeiterfassung2.setup.ISettings;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.SettingsActivity;
import askanimus.arbeitszeiterfassung2.widget.Widget;
import askanimus.arbeitszeiterfassung2.widget.WidgetConfigureActivity;

/**
 * @author askanimus@gmail.com on 19.08.15.
 */
public class ArbeitsplatzFragment
        extends
        Fragment
        implements
        View.OnClickListener,
        ArbeitsplatzExpandListAdapter.ArbeitsplatzExpandListeCallbacks
{

    private Context mContext;

    // Button zum hinzufügen eines neuen Arbeitsplatzes
    private ImageView bAddJob;

    // die Variablen der Jobliste
    private ExpandableListView mListe ;
    private ArbeitsplatzExpandListAdapter myAdapter;
    /*
     * Neue Instanz anlegen
     */
    public static ArbeitsplatzFragment newInstance(){

        return new ArbeitsplatzFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getContext();
        return inflater.inflate(R.layout.fragment_arbeitsplatz, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        ASettings.init(mContext, this::resume);

    }

    private void resume(){
        View mInhalt = getView();
        if(mInhalt != null) {
            bAddJob = Objects.requireNonNull(mInhalt).findViewById(R.id.A_add_job);

            myAdapter = new ArbeitsplatzExpandListAdapter(mContext, this);

            mListe = mInhalt.findViewById(R.id.A_liste);
            mListe.setAdapter(myAdapter);

            // handler für das öffnen der Kindelemente, nur eins soll zur gleichen Zeit offen sein
            mListe.expandGroup(0);
            mListe.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                int previousGroup = 0;

                @Override
                public void onGroupExpand(int groupPosition) {
                    if (groupPosition != previousGroup)
                        mListe.collapseGroup(previousGroup);
                    previousGroup = groupPosition;
                }
            });

            bAddJob.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == bAddJob.getId()){
            // neuen arbeitsplatz anlegen
            //Arbeitsplatz mJob = new Arbeitsplatz(-1);
            Arbeitsplatz mJob = ASettings.jobListe.add();
            // Liste neu laden
            myAdapter.reloadListe();
            myAdapter.notifyDataSetChanged();
            Intent iAddJob = new Intent();
            iAddJob.setClass(mContext, SettingsActivity.class);
            iAddJob.putExtra(ISettings.KEY_EDIT_JOB, mJob.getId());
            iAddJob.putExtra(ISettings.KEY_INIT_SEITE, ISettings.SETUP_SEITE_ARBEITSPLATZ);
            iAddJob.putExtra(ISettings.ARG_IS_INITASSIST, true);
            iAddJob.putExtra(ISettings.ARG_NUR_JOB, true);
            iAddJob.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(iAddJob);
        }
    }

    @Override
    public void onArbeitsplatzDelete(int index) {
        final Arbeitsplatz mJob = ASettings.jobListe.getVonIndex(index);

        if (mContext != null) {
            new AlertDialog.Builder(mContext)
                    .setTitle(getString(R.string.dialog_delete, mJob.getName()))
                    .setMessage(getString(R.string.dialog_delete_arbeitsplatz_frage, mJob.getName()))
                    .setPositiveButton(
                            mContext.getString(android.R.string.ok),
                            (dialog, whichButton) -> {
                                ASettings.jobListe.remove(index);
                                onArbeitsplatzSelect(0);
                                // Liste neu laden
                                myAdapter.reloadListe();
                                myAdapter.notifyDataSetChanged();
                                // das zugehörige Widge löschen
                                AppWidgetManager wm = AppWidgetManager.getInstance(mContext);
                                ComponentName cn = new ComponentName(mContext.getPackageName(), Widget.class.getName());
                                int[] widgetIds = wm.getAppWidgetIds(cn);
                                for (int id : widgetIds ) {
                                    Widget.updateAppWidget(mContext, wm, id);
                                    //WidgetConfigureActivity.loadPref(mContext, id, null);
                                }
                            })
                    .setNegativeButton(
                            mContext.getString(android.R.string.cancel),
                            (dialog, whichButton) -> {
                                // Do nothing.
                            }).show();
        }
    }

    @Override
    public void onArbeitsplatzSelect(long ArbeitsplatzID) {
        // neuen Arbeitsplatz als aktuellen öffnen
        ASettings.aktJob = ASettings.setAktivJob(ArbeitsplatzID);
            Activity mActivity = getActivity();
            if (mActivity != null) {
                Intent iMain = new Intent();
                iMain.putExtra(ISettings.KEY_JOBID, ArbeitsplatzID);
                iMain.setClass(mActivity, MainActivity.class);
                startActivity(iMain);
                mActivity.finish();
            }
        //}
    }

    @Override
    public void onArbeitsplatzEdit(long ArbeitsplatzID) {
        // Die Arbeitsplatzeinstellungen aufrufen
        Activity mActivity = getActivity();
        if(mActivity != null) {
            Intent iSettings = new Intent();
            iSettings.setClass(mActivity, SettingsActivity.class);
            iSettings.putExtra(ISettings.KEY_EDIT_JOB, ArbeitsplatzID);
            iSettings.putExtra(ISettings.KEY_INIT_SEITE, ISettings.SETUP_SEITE_ARBEITSPLATZ);
            iSettings.putExtra(ISettings.ARG_NUR_JOB, true);
            startActivity(iSettings);
            mActivity.finish();
        }
    }

    public void onArbeitsplatzKlone(long ArbeitsplatzID) {
        //Arbeitsplatz ArbeitsplatzKlone = new Arbeitsplatz(ArbeitsplatzID, true);
        Arbeitsplatz ArbeitsplatzKlone = ASettings.jobListe.getVonID(ArbeitsplatzID).klone();
        ASettings.jobListe.add(ArbeitsplatzKlone);

        ASettings.setAktivJob(ArbeitsplatzKlone.getId());

        // die Arbeitsplatzeinstellungen aufrufen
        Activity mActivity = getActivity();
        if(mActivity != null) {
            Intent iSettings = new Intent();
            iSettings.setClass(mActivity, SettingsActivity.class);
            iSettings.putExtra(ISettings.KEY_EDIT_JOB, ArbeitsplatzKlone.getId());
            iSettings.putExtra(ISettings.KEY_INIT_SEITE, ISettings.SETUP_SEITE_ARBEITSPLATZ);
            iSettings.putExtra(ISettings.ARG_NUR_JOB, true);
            startActivity(iSettings);
            mActivity.finish();
        }
    }
}
