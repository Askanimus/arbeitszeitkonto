/* Copyright 2014-2023 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitstag;

import android.app.Activity;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.view.OneShotPreDrawListener;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import androidx.appcompat.app.AlertDialog;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import askanimus.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import askanimus.betterpickers.calendardatepicker.MonthAdapter;

import java.util.Calendar;

import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitsmonat.Arbeitsmonat;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.ArbeitsplatzListAdapter;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASettings;
import askanimus.arbeitszeiterfassung2.setup.ISettings;

import static askanimus.arbeitszeiterfassung2.setup.ASettings.*;

/**
 * @author askanimus@gmail.com on 26.08.15.
 */
public class ArbeitstagPager  extends Fragment{
    static int KOPIE_EINZELN = 0;
    static int KOPIE_PERIODE = 1;

    private Context mContext;

    private DayPagerAdapter mPagerAdapter;
    private ViewPager2 mViewPager;
    private TextView tMonat;

    private static ArbeitstagFragment.ArbeitstagMainCallbacks mCallbackMain;

    /*
     * Neue Instanz anlegen
     */
    public static ArbeitstagPager newInstance(long datum, ArbeitstagFragment.ArbeitstagMainCallbacks callbacks) {
        mCallbackMain = callbacks;
        ArbeitstagPager fragment = new ArbeitstagPager();
        Bundle bundle = new Bundle();

        bundle.putLong(ISettings.ARG_DATUM, datum);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getContext();
        return inflater.inflate(R.layout.fragment_pager, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ASettings.init(mContext, this::resume);
    }

    private void resume(){
        final Datum kDatum;

        Bundle mArgs = getArguments();
        if (mArgs != null)
            kDatum = new Datum(mArgs.getLong(ISettings.ARG_DATUM), ASettings.aktJob.getWochenbeginn());
        else
            kDatum = new Datum(ASettings.aktDatum.getCalendar(), ASettings.aktJob.getWochenbeginn());


        // Werte der Kopfzeile eintragen
        View mInhalt = getView();
        if(mInhalt != null) {
            //TextView tJob = mInhalt.findViewById(R.id.P_wert_job);
            tMonat = mInhalt.findViewById(R.id.P_wert_monat);
            LinearLayout bKopf = mInhalt.findViewById(R.id.P_box_kopf);

            tMonat.setTextColor(ASettings.aktJob.getFarbe_Schrift_Titel());

            bKopf.setBackgroundColor(ASettings.aktJob.getFarbe());

            setMonat(kDatum);

            // Erzeugt den Adapter der für jede Seite entsprechnd der Seitennummer
            // den dazu gehörenden Tag als Fragment einbindet.
            mPagerAdapter = new DayPagerAdapter(this);

            // Der View-Pager
            mViewPager = mInhalt.findViewById(R.id.pager);
            mViewPager.setAdapter(mPagerAdapter);
            //mViewPager.setOffscreenPageLimit(1);

            mViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    //Datum kDat = new Datum(ASettings.aktJob.getStartDatum().getTimeInMillis(), ASettings.aktJob.getWochenbeginn());
                    kDatum.set(ASettings.aktJob.getStartDatum().getDate());
                    kDatum.add(Calendar.DAY_OF_MONTH, position);
                    setMonat(kDatum);
                    // Datum der letzten Ansicht speichern
                    SharedPreferences.Editor mEdit = ASettings.mPreferenzen.edit();
                    mEdit.putLong(ISettings.KEY_ANZEIGE_DATUM, kDatum.getTimeInMillis());
                    mEdit.apply();

                    //kDatum.set(kDat.getTime());
                }
            });

            // Anzuzeigenden Tag wählen 1-31
            //Datum mKalender = new Datum(ASettings.aktJob.getStartDatum().getTimeInMillis(), ASettings.aktJob.getWochenbeginn());
            int mPosition = ASettings.aktJob.getStartDatum().tageBis(kDatum);
            OneShotPreDrawListener.add(
                    mViewPager, () -> mViewPager.setCurrentItem(mPosition,false)
            );
            //mViewPager.setCurrentItem(mPosition, true);


            // Die Arbeitsplatzliste
            AppCompatSpinner sJobs = mInhalt.findViewById(R.id.P_wert_job);

            // der Adapter zum wechseln des Arbeitsplatzes
            final ArbeitsplatzListAdapter jobListeAdapter = new ArbeitsplatzListAdapter(mContext);
            sJobs.setAdapter(jobListeAdapter);
            sJobs.setSelection(0);
            sJobs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    long ArbeitsplatzID = jobListeAdapter.getItemId(i);
                    if (ArbeitsplatzID != ASettings.aktJob.getId()) {

                        Activity mActivity = getActivity();
                        if (mActivity != null) {
                            SharedPreferences.Editor mEdit = ASettings.mPreferenzen.edit();
                            mEdit.putLong(ISettings.KEY_JOBID, ArbeitsplatzID).apply();
                            Intent mainIntent = new Intent();
                            mainIntent.setClass(mActivity, MainActivity.class);
                            mainIntent.putExtra(ISettings.KEY_JOBID, ArbeitsplatzID);
                            mainIntent.putExtra(ISettings.KEY_ANZEIGE_VIEW, ISettings.VIEW_TAG);
                            mainIntent.putExtra(ISettings.KEY_ANZEIGE_DATUM, kDatum.getTimeInMillis());
                            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            //mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(mainIntent);
                            mActivity.finish();
                        }

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    private void setMonat(Datum datum ){
        tMonat.setText(datum.getString_Monat_Jahr(1, false));
    }

    /**
     * A {@link FragmentStateAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class DayPagerAdapter
            extends FragmentStateAdapter
            implements ArbeitstagFragment.ArbeitstagCallbacks, CalendarDatePickerDialogFragment.OnDateSetListener {
        private int kopie_methode = -1;

        int mSeiten;

        DayPagerAdapter(Fragment f) {
            super(f);
            mSeiten = ASettings.aktJob.getStartDatum().tageBis(ASettings.letzterAnzeigeTag)+1;
            if (mSeiten < 1) mSeiten = 1;
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            // getItem is called to instantiate the fragment for the given page.
            Datum mDatum = new Datum(ASettings.aktJob.getStartDatum());
            mDatum.add(Calendar.DAY_OF_MONTH, position);
            ArbeitstagFragment af = ArbeitstagFragment.newInstance(mDatum);
            af.setUp(this, mCallbackMain);
            return af;
        }

        @Override
        public int getItemCount() {
            return mSeiten;
        }


        /**
         * Neuberechnen der Monate, die zwischen den beiden Daten liegen
         * wenn ein einzelner Monat aktullisiert werden soll, dann leeres Enddatum übergeben
         * @param tagStart erster Monat der aktuallisiert werden soll
         * @param tagEnd letzter Monat deer aktuallisiert werden soll
         *               oder NULL wenn nur der erste Monat aktuallsiert werden soll
         */
        @Override
        public void onArbeitstagDatenChanged(Datum tagStart, Datum tagEnd) {
            Handler mHandler = new Handler();
            new Thread(() -> {
                Arbeitsmonat mMonat;
                Datum mTagEnd;
                Datum mTagStart = ASettings.aktJob.getAbrechnungsmonat(tagStart);
                mTagStart.setTag(aktJob.getMonatsbeginn());

                 /*Arbeitsmonat mMonat = new Arbeitsmonat(
                        ASettings.aktJob,
                        mTagStart.get(Calendar.YEAR),
                        mTagStart.get(Calendar.MONTH),
                        true,
                        true);
                mMonat.updateSollStunden();
                mMonat.updateSaldo(true);*/

                // wenn kein Enddatum angegeben, dann ist Start = Endmonat
                if (tagEnd != null) {
                    mTagEnd = ASettings.aktJob.getAbrechnungsmonat(tagEnd);
                    mTagEnd.setTag(aktJob.getMonatsbeginn());
                } else {
                    mTagEnd = new Datum(mTagStart);
                }
                // alle Monate berechnen, die von den beiden Tagen betroffen sind
                while (!mTagStart.liegtNach(mTagEnd)) {
                    mMonat = new Arbeitsmonat(
                            ASettings.aktJob,
                            mTagStart.get(Calendar.YEAR),
                            mTagStart.get(Calendar.MONTH),
                            true,
                            true);
                    mMonat.updateSollStunden();
                    mMonat.updateSaldo(true);

                    mTagStart.add(Calendar.MONTH, 1);
                }

                // Zur Datensicherung bei Google anmelden
                mHandler.post(ArbeitstagPager.this::requestBackup);
            }).start();
        }

        @Override
        public void onArbeitstagCopy(Datum tag, int methode) {
            kopie_methode = methode;
            Datum mTag = new Datum(tag);
            mTag.add(Calendar.DAY_OF_MONTH, 1);

            CalendarDatePickerDialogFragment bisKalenderPicker =
                    new CalendarDatePickerDialogFragment()
                            .setOnDateSetListener(this)
                            .setFirstDayOfWeek(ASettings.aktJob.getWochenbeginn())
                            .setPreselectedDate(
                                    mTag.get(Calendar.YEAR),
                                    mTag.get(Calendar.MONTH) - 1,
                                    mTag.get(Calendar.DAY_OF_MONTH));
            bisKalenderPicker.setDateRange(
                    new MonthAdapter.CalendarDay(
                            mTag.get(Calendar.YEAR),
                            mTag.get(Calendar.MONTH) - 1,
                            mTag.get(Calendar.DAY_OF_MONTH)),
                    new MonthAdapter.CalendarDay(
                            ASettings.letzterAnzeigeTag.get(Calendar.YEAR),
                            ASettings.letzterAnzeigeTag.get(Calendar.MONTH) - 1,
                            ASettings.letzterAnzeigeTag.get(Calendar.DAY_OF_MONTH)));
            if (isThemaDunkel)
                bisKalenderPicker.setThemeDark();
            else
                bisKalenderPicker.setThemeLight();
            FragmentManager fManager;
            try {
                fManager = getParentFragmentManager();
                bisKalenderPicker.show(fManager, getString(R.string.ende_titel));
            } catch (IllegalStateException ignore) {
            }
        }

        @Override
        public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
            final Datum datVon = new Datum(dialog.getMinDate().getDateInMillis(), ASettings.aktJob.getWochenbeginn());
            datVon.add(Calendar.DAY_OF_MONTH, -1);
            final Datum datBis = new Datum(year, monthOfYear + 1, dayOfMonth, ASettings.aktJob.getWochenbeginn());
            //final Datum datErsterZieltag = new Datum(datVon);

            // Fragen, ob die Aktion gewünscht ist
            Context context = getActivity();

            if (kopie_methode == KOPIE_PERIODE) {
                if (context != null) {
                    new AlertDialog.Builder(context)
                            .setTitle(getString(R.string.dialog_tage_kopie_periode))
                            .setMessage(getString(R.string.dialog_tage_kopie_frage_periode,
                                    datVon.getString_Datum(mContext),
                                    datBis.getString_Datum(mContext)))
                            .setPositiveButton(getString(android.R.string.yes), (dialog1, whichButton) -> {
                                Arbeitsmonat mMonat;
                                int mArbeitstag;

                                int mTage = datVon.tageBis(datBis);

                                // den Monat des Quelltages öffnen
                                // den Monat vorher auf den Abrechnungsmonat anpassen
                                Datum datumQuellmonat = ASettings.aktJob.getAbrechnungsmonat(datVon);
                                mMonat = new Arbeitsmonat(
                                        ASettings.aktJob,
                                        datumQuellmonat.get(Calendar.YEAR),
                                        datumQuellmonat.get(Calendar.MONTH), true, false);

                                // der Tag aus dem die Daten stammen
                                Arbeitstag mQuellTag = mMonat.getTagimMonat(datVon.get(Calendar.DAY_OF_MONTH));
                                // die Tage in welchen die Daten gkopiert werden
                                Arbeitstag mZielTag;

                                // Datum ab dem nach der Kopieraktion alle Tage und Monate aktuallisiert werden sollen
                                Datum datErsterZieltag = new Datum(datVon);
                                datErsterZieltag.add(Calendar.DAY_OF_MONTH, 1);

                                //Aktion ausführen
                                while (datVon.liegtVor(datBis)) {
                                    // zum nächsten Zieltag springen
                                    datVon.add(Calendar.DAY_OF_MONTH, 1);
                                    // liegt dieser Tag im nächsten Monat, aktuellen Monat neu berechnen und den nächsten öffnen
                                    if (datVon.get(Calendar.DAY_OF_MONTH) == ASettings.aktJob.getMonatsbeginn()) {
                                        mMonat = new Arbeitsmonat(
                                                ASettings.aktJob,
                                                datVon.get(Calendar.YEAR),
                                                datVon.get(Calendar.MONTH), true, false);
                                    }
                                    // Zieltag lesen
                                    mZielTag = mMonat.getTagimMonat(datVon.get(Calendar.DAY_OF_MONTH));

                                    if (mZielTag != null) {
                                        mArbeitstag = (int) (ASettings.aktJob.getArbeitstag(mZielTag.getKalender().get(Calendar.DAY_OF_WEEK)) * 2);

                                        // Ist der Zieltag kein definierter Ruhetag (halb oder ganz)
                                        // dann Quellschicht kopieren
                                        // sonst Ruhetag eintragen und restliche Schichten löschen
                                        if (mArbeitstag >= 2) {
                                            // alte Schichten löschen
                                            for (int i = mZielTag.getSchichtAnzahl(); i > 0; i--) {
                                                mZielTag.loescheSchicht(i - 1);
                                            }
                                            // Daten übertragen und speichern
                                            for (int i = 0; i < mQuellTag.getSchichtAnzahl(); i++) {
                                                if (ASettings.aktJob.isTeilschicht() ||
                                                        mQuellTag.getSchicht(i).getAbwesenheit().getKategorie() != Abwesenheit.KAT_KEINESCHICHT)
                                                    mZielTag.copySchicht(mQuellTag.getSchicht(i));
                                            }
                                        } else {
                                            mZielTag.getSchicht(0).setAbwesenheit(
                                                    ASettings.aktJob.getAbwesenheiten().getAktive(Abwesenheit.RUHETAG),
                                                    mArbeitstag == 0 ? 100 : 50);
                                            for (int i = mZielTag.getSchichtAnzahl(); i > 1; i--) {
                                                mZielTag.loescheSchicht(i - 1);
                                            }
                                        }
                                    }
                                }
                                // Update des Saldos des ersten Zieltages und aller nachfolgenden
                                // inkl. der Berechnungen der dazugehörigen Monate
                                try {
                                    onArbeitstagDatenChanged(datErsterZieltag, datBis);
                                } catch (NullPointerException ne) {
                                    ne.printStackTrace();
                                }
                                OneShotPreDrawListener.add(
                                        mViewPager,
                                        () -> {
                                            int seite = mViewPager.getCurrentItem() + mTage;
                                            mViewPager.setCurrentItem(
                                                    seite,
                                                    false);
                                            mPagerAdapter.notifyItemChanged(seite);
                                        }
                                );
                                /*mViewPager.setCurrentItem(
                                        mViewPager.getCurrentItem() + mTage, false);*/
                            })
                            .setNegativeButton(getString(android.R.string.no), (dialog14, whichButton) -> {
                                // Nichts tun, nur den Dialog schliessen.
                            }).show();
                }
            } else if (kopie_methode == KOPIE_EINZELN) {
                if (context != null) {
                    new AlertDialog.Builder(context)
                            .setTitle(getString(R.string.dialog_tage_kopie_einzel))
                            .setMessage(getString(R.string.dialog_tage_kopie_frage_einzel,
                                    datVon.getString_Datum(mContext),
                                    datBis.getString_Datum(mContext)))
                            .setPositiveButton(getString(android.R.string.yes), (dialog12, whichButton) -> {
                                Arbeitsmonat mMonat;
                                int mArbeitstag;

                                int mTage = datVon.tageBis(datBis);

                                //Quelltag finden
                                Datum d = ASettings.aktJob.getAbrechnungsmonat(datVon);
                                mMonat = new Arbeitsmonat(
                                        ASettings.aktJob,
                                        d.get(Calendar.YEAR),
                                        d.get(Calendar.MONTH),
                                        true, false);

                                Arbeitstag mQuellTag = mMonat.getTagimMonat(datVon.get(Calendar.DAY_OF_MONTH));

                                // Zieltag finden
                                d = ASettings.aktJob.getAbrechnungsmonat(datBis);
                                mMonat = new Arbeitsmonat(
                                        ASettings.aktJob,
                                        d.get(Calendar.YEAR),
                                        d.get(Calendar.MONTH), true, false);
                                Arbeitstag mZielTag = mMonat.getTagimMonat(datBis.get(Calendar.DAY_OF_MONTH));

                                // Schichten kopieren
                                if (mZielTag != null) {
                                    mArbeitstag = (int) (ASettings.aktJob.getArbeitstag(mZielTag.getKalender().get(Calendar.DAY_OF_WEEK)) * 2);

                                    // Ist der Zieltag kein definierter Ruhetag (halb oder ganz)
                                    // dann Quellschicht kopieren
                                    // sonst Ruhetag eintragen und restliche Schichten löschen
                                    if (mArbeitstag >= 2) {
                                        // alte Schichten löschen
                                        for (int i = mZielTag.getSchichtAnzahl(); i > 0; i--) {
                                            mZielTag.loescheSchicht(i - 1);
                                        }
                                        // Daten übertragen und speichern
                                        for (int i = 0; i < mQuellTag.getSchichtAnzahl(); i++) {
                                            if (ASettings.aktJob.isTeilschicht() ||
                                                    mQuellTag.getSchicht(i).getAbwesenheit().getKategorie() != Abwesenheit.KAT_KEINESCHICHT)
                                                mZielTag.copySchicht(mQuellTag.getSchicht(i));
                                        }
                                    } else {
                                        mZielTag.getSchicht(0).setAbwesenheit(
                                                ASettings.aktJob.getAbwesenheiten().getAktive(Abwesenheit.RUHETAG),
                                                mArbeitstag == 0 ? 100 : 50);
                                        for (int i = mZielTag.getSchichtAnzahl(); i > 1; i--) {
                                            mZielTag.loescheSchicht(i - 1);
                                        }
                                    }

                                    // den Zielmonat und alle folgenden aktuallisieren
                                    onArbeitstagDatenChanged(mZielTag.getKalender(), null);


                                    // Zieltag öffnen
                                    //mPagerAdapter.notifyDataSetChanged();
                                    OneShotPreDrawListener.add(
                                            mViewPager,
                                            () -> {
                                                int seite = mViewPager.getCurrentItem() + mTage;
                                                mViewPager.setCurrentItem(
                                                        seite,
                                                        true);
                                                mPagerAdapter.notifyItemChanged(seite);
                                            }
                                    );
                                    //mViewPager.setCurrentItem(mViewPager.getCurrentItem() + mTage, true);

                                    // Datenbackup im Googleprofil anregen
                                    // requestBackup();
                                }
                            })
                            .setNegativeButton(getString(android.R.string.no), (dialog13, whichButton) -> {
                                // Nichts tun, nur den Dialog schliessen.
                            }).show();
                }
            }
            kopie_methode = -1;
        }
    }

    // Backup im Google Konto anfordern
    private void requestBackup() {
        BackupManager bm = new BackupManager(getActivity());
        try {
            bm.dataChanged();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }
}
