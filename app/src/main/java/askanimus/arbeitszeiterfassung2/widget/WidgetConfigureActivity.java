/* Copyright 2014-2023 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.CompoundButtonCompat;
import androidx.preference.PreferenceManager;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Objects;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.ArbeitsplatzListAdapter;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.ArbeitsplatzListe;
import askanimus.arbeitszeiterfassung2.setup.ASettings;
import askanimus.arbeitszeiterfassung2.setup.ISettings;
import askanimus.betterpickers.numberpicker.NumberPickerBuilder;
import askanimus.betterpickers.numberpicker.NumberPickerDialogFragment;

/**
 * The configuration screen for the {@link Widget Stempeluhr} AppWidget.
 */
public class WidgetConfigureActivity
        extends
        AppCompatActivity
        implements
        ISettings,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2{

    private RadioGroup gRundenStart;
    private AppCompatEditText tRundenStart;
    private RadioGroup gRundenEnd;
    private AppCompatEditText tRundenEnd;
    private RadioGroup gRundenPause;
    private AppCompatEditText tRundenPause;
    private WidgetStatus wStatus;

    /*public StempeluhrConfigureActivity() {
        super();
    }*/

    // Zeigt ob es ein bestehendes Widget ist das konfiguriert wird
    private Boolean isNotNew = false;

    // Laden der Einstellungenn des betreffenden Widgets
    public static WidgetStatus loadPref(Context context, int appWidgetId, String action) {
        WidgetStatus mStatus = null;
        SharedPreferences mPrefs;
       if(ASettings.zustand == ISettings.INIT_ZUSTAND_GELADEN) {
           if(ASettings.mPreferenzen.contains(WidgetStatus.KEY_JOB + appWidgetId)){
               mPrefs = ASettings.mPreferenzen;
           } else {
               // vor Vers. 2.00.00 wurden die Einstellungen im anderen Context gespeichert
               mPrefs = context.getSharedPreferences(WidgetStatus.PREFS_NAME, MODE_PRIVATE);
           }

           // testen ob der Arbeitsplatz noch existiert
           long arbeitsplatzID = mPrefs.getLong(WidgetStatus.KEY_JOB + appWidgetId, 0);
           if(arbeitsplatzID > 0) {
               Arbeitsplatz arbeitsplatz = ASettings.getArbeitsplatz(arbeitsplatzID);
               if (arbeitsplatz != null){
                   mStatus = new WidgetStatus(arbeitsplatz, appWidgetId, "");
                   mStatus.load(mPrefs);
               } else {
                   deletePref(context, appWidgetId);
                   AppWidgetHost host = new AppWidgetHost(context, 0);
                   host.deleteAppWidgetId(appWidgetId);
               }
           }


           /*if(arbeitsplatzID > 0) {
               String sql = "select " +
                       Datenbank.DB_F_ID +
                       " from " +
                       Datenbank.DB_T_JOB +
                       " where " +
                       Datenbank.DB_F_ID +
                       " = " +
                       arbeitsplatzID +
                       " LIMIT 1 ";
               Cursor result = ASettings.mDatenbank.rawQuery(sql, null);

               // Resultat der Anfrage auswerten
               if (result.getCount() > 0) {
                   try {
                       mStatus = new WidgetStatus(
                               new Arbeitsplatz(arbeitsplatzID),
                               appWidgetId);

                       mStatus.load(mPrefs);
                   } catch (RuntimeException e) {
                       if (mStatus != null) {
                           mStatus.delete(ASettings.mPreferenzen, appWidgetId);
                       }
                       mStatus = null;
                       result.close();
                   }
                   result.close();
               } else {
                   result.close();
                   deletePref(context, appWidgetId);
               }
           }*/
       } else {
           ASettings.init(context, () -> {
               loadPref(context, appWidgetId,action);
               if(action != null) {
                   try {
                       Widget.getPendingSelfIntent(context, action, appWidgetId).send();
                   } catch (PendingIntent.CanceledException e) {
                       e.printStackTrace();
                   }
               }
           });
           /*ASettings.init(context, () -> {
            if(action != null) {
                try {
                    Widget.getPendingSelfIntent(context, action, appWidgetId).send();
                } catch (PendingIntent.CanceledException e) {
                    e.printStackTrace();
                }
            }

           });*/
       }
        return mStatus;
    }

    static void deletePref(Context context, int id) {
        if (ASettings.zustand == ISettings.INIT_ZUSTAND_GELADEN) {
        SharedPreferences.Editor mPrefs = ASettings.mPreferenzen.edit();
        mPrefs.remove(WidgetStatus.KEY_JOB + id);
        mPrefs.remove(WidgetStatus.KEY_STATUS + id);
        mPrefs.remove(WidgetStatus.KEY_DATUM + id);
        mPrefs.remove(WidgetStatus.KEY_ZEIT + id);
        mPrefs.remove(WidgetStatus.KEY_PAUSE_BEGINN + id);
        mPrefs.remove(WidgetStatus.KEY_PAUSESUMME + id);
        mPrefs.remove(WidgetStatus.KEY_OPT_OPTIONEN + id);
        mPrefs.remove(WidgetStatus.KEY_OPT_RUNDEN + id);
        mPrefs.remove(WidgetStatus.KEY_OPT_RUNDEN_MINUTEN + id);
        mPrefs.remove(WidgetStatus.KEY_OPT_RUNDEN_VON + id);
        mPrefs.remove(WidgetStatus.KEY_OPT_RUNDEN_VON_MINUTEN + id);
        mPrefs.remove(WidgetStatus.KEY_OPT_RUNDEN_BIS + id);
        mPrefs.remove(WidgetStatus.KEY_OPT_RUNDEN_BIS_MINUTEN + id);
        mPrefs.remove(WidgetStatus.KEY_OPT_RUNDEN_PAUSE + id);
        mPrefs.remove(WidgetStatus.KEY_OPT_RUNDEN_PAUSE_MINUTEN + id);
        // mPrefs.remove(KEY_FIXSCHICHT + id);

        int i = 0;
        while (ASettings.mPreferenzen.contains(i + WidgetStatus.KEY_PAUSE_BEGINN + id)) {
            mPrefs.remove(i + WidgetStatus.KEY_PAUSE_BEGINN + id);
            mPrefs.remove(i + WidgetStatus.KEY_PAUSE_LAENGE + id);
            i++;
        }

        i = 0;
        while (ASettings.mPreferenzen.contains(i + WidgetStatus.KEY_TIMER_MIN + id)) {
            mPrefs.remove(i + WidgetStatus.KEY_TIMER_MIN + id);
            mPrefs.remove(i + WidgetStatus.KEY_TIMER_TEXT + id);
            mPrefs.remove(i + WidgetStatus.KEY_TIMER_SOUND + id);
            i++;
        }
        mPrefs.apply();
        } else {
            ASettings.init(context, () -> deletePref(context, id));
        }
    }


    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getBoolean(ASettings.KEY_THEMA_DUNKEL, false) ?
                        R.style.MyFullscreenTheme :
                        R.style.MyFullscreenTheme_Light
        );

        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                BackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ASettings.init(this, this::resume);
    }

    private void resume() {
        setTheme(ASettings.isThemaDunkel ? R.style.MyFullscreenTheme : R.style.MyFullscreenTheme_Light);

        final AppCompatSpinner mSpinnerJob;

        final ArbeitsplatzListe mJobs = ASettings.jobListe;

        int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;


        // die Widget Id auslesen
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    Widget.KEY_WIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            isNotNew = extras.getBoolean(Widget.KEY_WIDGET_CONFIG, false);
        }

        // Wurde die App ohne eine Appwidget ID aufgerufen?
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
            return;
        }

        SharedPreferences mPrefs = ASettings.mPreferenzen;
        if (!mPrefs.contains(WidgetStatus.KEY_JOB + mAppWidgetId)) {
            SharedPreferences.Editor pEdit = mPrefs.edit();
            pEdit.putLong(WidgetStatus.KEY_JOB + mAppWidgetId, ASettings.aktJob.getId()).apply();
        }

        wStatus = loadPref(this, mAppWidgetId, null);

        if (wStatus != null) {
            // Set the result to CANCELED.  This will cause the widget host to cancel
            // out of the widget placement if the user presses the back button.
            setResult(RESULT_CANCELED);

            setContentView(R.layout.widget_configure);


            mSpinnerJob = findViewById(R.id.SW_spinner_job);

            final ArbeitsplatzListAdapter jobListeAdapter = new ArbeitsplatzListAdapter(this);
            mSpinnerJob.setAdapter(jobListeAdapter);
            mSpinnerJob.setSelection(mJobs.getIndex(wStatus.mJob.getId()));
            mSpinnerJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    long mID = mJobs.getVonIndex(position).getId();
                    if (wStatus.mJob.getId() != mID/*mJobs.getID(position)*/) {
                        wStatus.mJob = mJobs.getVonID(mID);//new Arbeitsplatz(mJobs.getID(position));

                        wStatus.save(mPrefs);
                        updateButtons(true);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            updateButtons(false);
        }
    }

    private void updateButtons(boolean isChage) {
        // Farben setzen

        // Werte setzen und optionen ausblenden
        // Soll der Tag nach dem Ausstempeln geöffnet werden?
        SwitchCompat sOpenTag = findViewById(R.id.SW_switch_opentag);
        sOpenTag.setThumbTintList(wStatus.mJob.getFarbe_Thumb());
        sOpenTag.setTrackTintList(wStatus.mJob.getFarbe_Trak());

        // Soll immer eine Extraschicht angelegt werden?
        SwitchCompat sExtraSchicht = findViewById(R.id.SW_switch_extraschicht);
        sExtraSchicht.setThumbTintList(wStatus.mJob.getFarbe_Thumb());
        sExtraSchicht.setTrackTintList(wStatus.mJob.getFarbe_Trak());

        // soll eine definierte Schcht an Hand der Zeiten erkannt werden?
        SwitchCompat sErkenneSchicht = findViewById(R.id.SW_switch_erkenne_schicht);
        sErkenneSchicht.setThumbTintList(wStatus.mJob.getFarbe_Thumb());
        sErkenneSchicht.setTrackTintList(wStatus.mJob.getFarbe_Trak());

        // definierte Werte der erkannten Schicht überschreiben
        // von
        AppCompatCheckBox mBox_von = findViewById(R.id.SW_opt_vonreal);
        CompoundButtonCompat.setButtonTintList(
                mBox_von,
                wStatus.mJob.getFarbe_Radio());
        //bis
        AppCompatCheckBox mBox_bis = findViewById(R.id.SW_opt_bisreal);
        CompoundButtonCompat.setButtonTintList(
                mBox_bis,
                wStatus.mJob.getFarbe_Radio());
        // Pause
        AppCompatCheckBox mBox_pause = findViewById(R.id.SW_opt_pausereal);
        CompoundButtonCompat.setButtonTintList(
                mBox_pause,
                wStatus.mJob.getFarbe_Radio());

        // Runden
        SwitchCompat sRunden = findViewById(R.id.SW_switch_runden);
        sRunden.setThumbTintList(wStatus.mJob.getFarbe_Thumb());
        sRunden.setTrackTintList(wStatus.mJob.getFarbe_Trak());

        // sollen die Startzeit gerundet werden?
        gRundenStart = findViewById(R.id.SW_group_runden_start);
        for (View v : gRundenStart.getTouchables()) {
            CompoundButtonCompat.setButtonTintList(
                    ((AppCompatRadioButton) v),
                    wStatus.mJob.getFarbe_Radio());
        }
        // auf welchen Wert soll die Startzeit gerundet werden
        tRundenStart = findViewById(R.id.SW_wert_runden_start);

        // sollen die Endzeit gerundet werden?
        gRundenEnd = findViewById(R.id.SW_group_runden_end);
        for (View v : gRundenEnd.getTouchables()) {
            CompoundButtonCompat.setButtonTintList(
                    ((AppCompatRadioButton) v),
                    wStatus.mJob.getFarbe_Radio());
        }
        // auf welchen Wert soll die Endzeit gerundet werden
        tRundenEnd = findViewById(R.id.SW_wert_runden_end);

        // sollen die Pause gerundet werden?
        gRundenPause = findViewById(R.id.SW_group_runden_pause);
        for (View v : gRundenPause.getTouchables()) {
            CompoundButtonCompat.setButtonTintList(
                    ((AppCompatRadioButton) v),
                    wStatus.mJob.getFarbe_Radio());
        }
        // auf welchen Wert soll die Pause gerundet werden
        tRundenPause = findViewById(R.id.SW_wert_runden_pause);

        // der Button zum übernehmen der Einstellungen
        AppCompatButton bOK = findViewById(R.id.SW_button_add);
        ViewCompat.setBackgroundTintList(bOK, wStatus.mJob.getFarbe_Button());
        bOK.setTextColor(wStatus.mJob.getFarbe_Schrift_Button());


        // wenn kein Wechsel des Arbeitsplatzes dann die Handler und Werte setzen
        if(!isChage){
            // Tag nach ausstempeln öffnen
            sOpenTag.setChecked(wStatus.isOptionSet(WidgetStatus.OPT_TAG_OEFFEN));
            // Extraschicht anlegen
            sExtraSchicht.setChecked(wStatus.isOptionSet(WidgetStatus.OPT_EXTRASCHICHT));

            // Schichterkennung
            if (!wStatus.isOptionSet(WidgetStatus.OPT_ERKENNE_SCHICHT)) {
                findViewById(R.id.SW_box_erkenne_schicht).setVisibility(View.GONE);
            } else {
                sErkenneSchicht.setChecked(true);
                if (wStatus.isOptionSet(WidgetStatus.OPT_EXTRASCHICHT))
                    findViewById(R.id.SW_warnung).setVisibility(View.GONE);
            }

            // Stempelwerte
            mBox_von.setChecked(wStatus.isOptionSet(WidgetStatus.OPT_SET_VON_REAL));
            mBox_bis.setChecked(wStatus.isOptionSet(WidgetStatus.OPT_SET_BIS_REAL));
            mBox_pause.setChecked(wStatus.isOptionSet(WidgetStatus.OPT_SET_PAUSE_REAL));

            // Runden
            if (wStatus.mRundenVon + wStatus.mRundenBis + wStatus.mRundenPause == WidgetStatus.NICHTRUNDEN) {
                // kein Runden gewünscht
                findViewById(R.id.SW_box_runden).setVisibility(View.GONE);
            } else {
                sRunden.setChecked(true);

                // Startzeit runden
                tRundenStart.setText(String.valueOf(wStatus.mRundenVon_minuten));
                switch (wStatus.mRundenVon) {
                    case WidgetStatus.AUFRUNDEN:
                        gRundenStart.check(R.id.SW_opt_aufrunden_start);
                        break;
                    case WidgetStatus.ABRUNDEN:
                        gRundenStart.check(R.id.SW_opt_abrunden_start);
                        break;
                    default:
                        gRundenStart.check(R.id.SW_opt_kaufrunden_start);
                }

                // Endzeit runden
                tRundenEnd.setText(String.valueOf(wStatus.mRundenBis_minuten));
                switch (wStatus.mRundenBis) {
                    case WidgetStatus.AUFRUNDEN:
                        gRundenEnd.check(R.id.SW_opt_aufrunden_end);
                        break;
                    case WidgetStatus.ABRUNDEN:
                        gRundenEnd.check(R.id.SW_opt_abrunden_end);
                        break;
                    default:
                        gRundenEnd.check(R.id.SW_opt_kaufrunden_end);
                }

                // Pause runden
                tRundenPause.setText(String.valueOf(wStatus.mRundenPause_minuten));
                switch (wStatus.mRundenPause) {
                    case WidgetStatus.AUFRUNDEN:
                        gRundenPause.check(R.id.SW_opt_aufrunden_pause);
                        break;
                    case WidgetStatus.ABRUNDEN:
                        gRundenPause.check(R.id.SW_opt_abrunden_pause);
                        break;
                    default:
                        gRundenPause.check(R.id.SW_opt_kaufrunden_pause);
                }
            }

            // Handler zuweisen
            sExtraSchicht.setOnCheckedChangeListener(mExtraSchichtListener);
            sErkenneSchicht.setOnCheckedChangeListener(mErkenneSchichtListener);
            sRunden.setOnCheckedChangeListener(mRundenListener);
            tRundenStart.setOnClickListener(mRundenClickListener);
            tRundenEnd.setOnClickListener(mRundenClickListener);
            tRundenPause.setOnClickListener(mRundenClickListener);

            // wenn es ein bestehendes Widget ist dann den Buttontext umstellen
            if (isNotNew) {
                bOK.setVisibility(View.GONE);
            } else {
                bOK.setOnClickListener(mOkClickListener);
            }

        }
    }

    /*
    * Handlerroutinen
    */
    View.OnClickListener mRundenClickListener = v -> {
        NumberPickerBuilder npb = new NumberPickerBuilder()
                .setFragmentManager(getSupportFragmentManager())
                .setMinNumber(BigDecimal.valueOf(1))
                .setMaxNumber(BigDecimal.valueOf(60))
                .setDecimalVisibility(View.INVISIBLE)
                .setPlusMinusVisibility(View.INVISIBLE)
                .setStyleResId(R.style.BetterPickersDialogFragment_Light)
                .setLabelText(getString(R.string.minutes_label))
                .setReference(v.getId());
        npb.show();
    };

    @Override
    public void onDialogNumberSet(int reference, BigInteger number, double decimal, boolean isNegative, BigDecimal fullNumber) {
        ((AppCompatEditText)findViewById(reference)).setText(String.format(Locale.getDefault(), "%d", number));
    }

     CompoundButton.OnCheckedChangeListener mErkenneSchichtListener =
             (buttonView, isChecked) -> findViewById(R.id.SW_box_erkenne_schicht).setVisibility( isChecked? View.VISIBLE: View.GONE);

     CompoundButton.OnCheckedChangeListener mExtraSchichtListener =
             (buttonView, isChecked) -> findViewById(R.id.SW_warnung).setVisibility( isChecked? View.GONE: View.VISIBLE);

    CompoundButton.OnCheckedChangeListener mRundenListener =
            (buttonView, isChecked) -> {
                //findViewById(R.id.SW_box_runden).setVisibility(isChecked ? View.VISIBLE : View.GONE);
                if (isChecked) {
                    findViewById(R.id.SW_box_runden).setVisibility(View.VISIBLE); // Startzeit runden
                    tRundenStart.setText(String.valueOf(wStatus.mRundenVon_minuten));
                    switch (wStatus.mRundenVon) {
                        case WidgetStatus.AUFRUNDEN:
                            gRundenStart.check(R.id.SW_opt_aufrunden_start);
                            break;
                        case WidgetStatus.ABRUNDEN:
                            gRundenStart.check(R.id.SW_opt_abrunden_start);
                            break;
                        default:
                            gRundenStart.check(R.id.SW_opt_kaufrunden_start);
                    }

                    // Endzeit runden
                    tRundenEnd.setText(String.valueOf(wStatus.mRundenBis_minuten));
                    switch (wStatus.mRundenBis) {
                        case WidgetStatus.AUFRUNDEN:
                            gRundenEnd.check(R.id.SW_opt_aufrunden_end);
                            break;
                        case WidgetStatus.ABRUNDEN:
                            gRundenEnd.check(R.id.SW_opt_abrunden_end);
                            break;
                        default:
                            gRundenEnd.check(R.id.SW_opt_kaufrunden_end);
                    }

                    // Pause runden
                    tRundenPause.setText(String.valueOf(wStatus.mRundenPause_minuten));
                    switch (wStatus.mRundenPause) {
                        case WidgetStatus.AUFRUNDEN:
                            gRundenPause.check(R.id.SW_opt_aufrunden_pause);
                            break;
                        case WidgetStatus.ABRUNDEN:
                            gRundenPause.check(R.id.SW_opt_abrunden_pause);
                            break;
                        default:
                            gRundenPause.check(R.id.SW_opt_kaufrunden_pause);
                    }
                } else {
                    findViewById(R.id.SW_box_runden).setVisibility(View.GONE);
                }
            };

    View.OnClickListener mOkClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Context context = WidgetConfigureActivity.this;

            // Buttons auswerten
            wStatus.setOption(WidgetStatus.OPT_TAG_OEFFEN, ((SwitchCompat) findViewById(R.id.SW_switch_opentag)).isChecked());
            wStatus.setOption(WidgetStatus.OPT_EXTRASCHICHT, ((SwitchCompat) findViewById(R.id.SW_switch_extraschicht)).isChecked());
            wStatus.setOption(WidgetStatus.OPT_ERKENNE_SCHICHT, ((SwitchCompat) findViewById(R.id.SW_switch_erkenne_schicht)).isChecked());
            wStatus.setOption(WidgetStatus.OPT_SET_VON_REAL, ((AppCompatCheckBox) findViewById(R.id.SW_opt_vonreal)).isChecked());
            wStatus.setOption(WidgetStatus.OPT_SET_BIS_REAL, ((AppCompatCheckBox) findViewById(R.id.SW_opt_bisreal)).isChecked());
            wStatus.setOption(WidgetStatus.OPT_SET_PAUSE_REAL, ((AppCompatCheckBox) findViewById(R.id.SW_opt_pausereal)).isChecked());
            if (((SwitchCompat) findViewById(R.id.SW_switch_runden)).isChecked()) {
                AppCompatEditText tMinuten;
                String m;

                // Startzeit runden
                tMinuten = findViewById(R.id.SW_wert_runden_start);
                m = Objects.requireNonNull(tMinuten.getText()).toString();
                wStatus.mRundenVon_minuten = Integer.parseInt(m);
                RadioGroup gRundenStart = findViewById(R.id.SW_group_runden_start);
                int checkedRadioButtonId = gRundenStart.getCheckedRadioButtonId();
                if (checkedRadioButtonId == R.id.SW_opt_aufrunden_start) {
                    wStatus.mRundenVon = WidgetStatus.AUFRUNDEN;
                } else if (checkedRadioButtonId == R.id.SW_opt_abrunden_start) {
                    wStatus.mRundenVon = WidgetStatus.ABRUNDEN;
                } else {
                    wStatus.mRundenVon = WidgetStatus.KAUFRUNDEN;
                }

                // Endzeit runden
                tMinuten = findViewById(R.id.SW_wert_runden_end);
                m = Objects.requireNonNull(tMinuten.getText()).toString();
                wStatus.mRundenBis_minuten = Integer.parseInt(m);
                RadioGroup gRundenStop = findViewById(R.id.SW_group_runden_end);
                checkedRadioButtonId = gRundenStop.getCheckedRadioButtonId();
                if (checkedRadioButtonId == R.id.SW_opt_aufrunden_end) {
                    wStatus.mRundenBis = WidgetStatus.AUFRUNDEN;
                } else if (checkedRadioButtonId == R.id.SW_opt_abrunden_end) {
                    wStatus.mRundenBis = WidgetStatus.ABRUNDEN;
                } else {
                    wStatus.mRundenBis = WidgetStatus.KAUFRUNDEN;
                }

                // Pause runden
                tMinuten = findViewById(R.id.SW_wert_runden_pause);
                m = Objects.requireNonNull(tMinuten.getText()).toString();
                wStatus.mRundenPause_minuten = Integer.parseInt(m);
                RadioGroup gRundenPause = findViewById(R.id.SW_group_runden_pause);
                checkedRadioButtonId = gRundenPause.getCheckedRadioButtonId();
                if (checkedRadioButtonId == R.id.SW_opt_aufrunden_pause) {
                    wStatus.mRundenPause = WidgetStatus.AUFRUNDEN;
                } else if (checkedRadioButtonId == R.id.SW_opt_abrunden_pause) {
                    wStatus.mRundenPause = WidgetStatus.ABRUNDEN;
                } else {
                    wStatus.mRundenPause = WidgetStatus.KAUFRUNDEN;
                }
            } else {
                wStatus.mRundenVon = WidgetStatus.NICHTRUNDEN;
                wStatus.mRundenVon_minuten = 1;
                wStatus.mRundenBis = WidgetStatus.NICHTRUNDEN;
                wStatus.mRundenBis_minuten = 1;
                wStatus.mRundenPause = WidgetStatus.NICHTRUNDEN;
                wStatus.mRundenPause_minuten = 1;
            }

            // When the button is clicked, store the string locally
            wStatus.save(ASettings.mPreferenzen);

            // It is the responsibility of the configuration activity to update the app widget
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

            Widget.updateAppWidget(context, appWidgetManager, (int)wStatus.mID);

            // Make sure we pass back the original appWidgetId
            Intent resultValue = new Intent();
            resultValue.putExtra(Widget.KEY_WIDGET_ID, wStatus.mID);
            setResult(RESULT_OK, resultValue);
            finish();
        }
    };

    private void BackPressed() {
        if (isNotNew) {
            mOkClickListener.onClick(findViewById(R.id.SW_button_add));
        }
        finish();
    }
}

