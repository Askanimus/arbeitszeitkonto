/* Copyright 2014-2023 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASettings;

class SchichtAuswahlListAdapter extends BaseAdapter {
    private final ArrayList<String> schichtNamenListe;
    private final AuswahlListeCallbacks callBack;

    protected SchichtAuswahlListAdapter(String[] liste, AuswahlListeCallbacks cb) {
        schichtNamenListe = new ArrayList<>();
        schichtNamenListe.add(ASettings.res.getString(
                R.string.ohne_vorlage,
                ASettings.res.getString(R.string.stempelzeit)
        ));
        schichtNamenListe.addAll(Arrays.asList(liste));
        callBack = cb;
    }

    @Override
    public int getCount() {
        return schichtNamenListe.size();
    }

    @Override
    public String getItem(int position) {
        return schichtNamenListe.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layInflator = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (layInflator != null) {
            convertView = layInflator.inflate(android.R.layout.simple_list_item_1, parent, false);

            TextView schichtView = convertView.findViewById(android.R.id.text1);
            schichtView.setText(getItem(position));

            schichtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(callBack != null){
                        callBack.onItemSelect(position -1);
                    }
                }
            });
        }
        return convertView;
    }

    /*
     * Callback Interfaces
     */
    public interface AuswahlListeCallbacks {
        /**
         * Aufgerufen wenn ein Listeneintrag angetippt wurde
         */
        void onItemSelect(int position);
    }
}