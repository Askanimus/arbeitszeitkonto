package askanimus.arbeitszeiterfassung2.widget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.preference.PreferenceManager;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASettings;
import askanimus.arbeitszeiterfassung2.setup.ISettings;


public class WidgetSchichtAuswahlActivity extends Activity
        implements ISettings, SchichtAuswahlListAdapter.AuswahlListeCallbacks {
    public final static String LISTE_SCHICHT_NAMEN = "schichtNamenliste";
    public final static String LISTE_SCHICHT_IDS = "schichtIdsliste";

    private int mWidgetId;
    private int mVon;
    private int mBis;
    private int mPause;
    private long[] schichtIDsListe;

    TextView tHint;
    SwitchCompat sAdd;
    TextView tHintADD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getBoolean(ASettings.KEY_THEMA_DUNKEL, false) ?
                        android.R.style.Theme_Material_Dialog :
                        android.R.style.Theme_Material_Light_Dialog
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Einstellungen laden, wenn noch nicht geschehen
        ASettings.init(this, this::resume);
    }

    private void resume() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mWidgetId = extras.getInt(
                    Widget.KEY_WIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID
            );
        }

        // Wurde die App ohne eine gültig Appwidget ID aufgerufen?
        // die Activity beenden
        if (mWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        } else {
            setContentView(R.layout.widget_schichtauswahl);


            // den betreffenden Arbeitsplatz suchen
            Arbeitsplatz mJob = ASettings.getArbeitsplatz(intent.getLongExtra(ISettings.KEY_JOBID, 0));

            if (mJob == null) {
                finish();
            } else {
                // Elemente suchen
                tHint = findViewById(R.id.SA_hint);
                sAdd = findViewById(R.id.SA_switch_extra);
                sAdd.setHintTextColor(mJob.getFarbe_Schrift_default());
                sAdd.setThumbTintList(mJob.getFarbe_Thumb());
                sAdd.setTrackTintList(mJob.getFarbe_Trak());
                tHintADD = findViewById(R.id.SA_hint_extra);

                        // die Textfarben
                        TextView tFrage = findViewById(R.id.SA_frage);
                        tFrage.setTextColor(mJob.getFarbe_Schrift_default());
                        tHint.setTextColor(mJob.getFarbe_Schrift_default());
                // den Schalter für Extraschichten einrichten
                sAdd.setChecked(intent.getBooleanExtra(Widget.KEY_ADDSCHICHT, false));
                if(sAdd.isChecked()){
                    sAdd.setVisibility(View.GONE);
                    tHintADD.setVisibility(View.GONE);
                } else {
                    sAdd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            tHintADD.setText(
                                    isChecked ?
                                    R.string.hint_add_extraschicht :
                                    R.string.hint_add_extraschicht_no
                            );
                        }
                    });
                }

                // den Arbeitsplatzname als Dialogtitel setzen
                setTitle(mJob.getName());

                // die Dialogfarben an die des Arbeitsplatzes anpassen
                View titleView = getWindow().findViewById(android.R.id.title);
                if (titleView != null) {
                    View parent = titleView.findViewById(R.id.SA_hintergrund);

                    if (parent != null) {
                        // die Dialog Hintergrundfarbe
                        parent.setBackgroundColor(mJob.getFarbe_Hintergrund());
                        // die Farbe des Titels - Arbeitsplatznamens
                        setTitleColor(mJob.getFarbe_Schrift_default());
                    }
                }
            }

            ListView schichtListeView = findViewById(R.id.SA_liste);

            // den Hinweistext anpassen
            int anzahl = intent.getIntExtra(Widget.KEY_DEFSCHICHT, 0);
            if (anzahl < 0) {
                tHint.setText(R.string.hint_schichtauswahl_no);
            } else if (anzahl == 0) {
                tHint.setText(R.string.hint_schichtauswahl_all);
            } else {
                tHint.setText(R.string.hint_schichtauswahl);
            }

            mVon = intent.getExtras().getInt(Widget.KEY_VON, 0);
            mBis = intent.getExtras().getInt(Widget.KEY_BIS, 0);
            mPause = intent.getExtras().getInt(Widget.KEY_PAUSE, 0);

            schichtIDsListe = intent.getExtras().getLongArray(LISTE_SCHICHT_IDS);

            BaseAdapter listAdapter = new SchichtAuswahlListAdapter(
                    intent.getExtras().getStringArray(LISTE_SCHICHT_NAMEN),
                    this
            );

            schichtListeView.setAdapter(listAdapter);
        }
    }

    @Override
    public void onItemSelect(int position) {
        // die ausgewählte defaultSchicht an das Widget senden
        Intent eintragenIntent = new Intent();
        eintragenIntent.setClass(getApplicationContext(), Widget.class);
        eintragenIntent.setAction(Widget.CLOSE_SCHICHTAUSWAHL);
        eintragenIntent.putExtra(Widget.KEY_WIDGET_ID, mWidgetId);
        eintragenIntent.putExtra(Widget.KEY_VON, mVon);
        eintragenIntent.putExtra(Widget.KEY_BIS, mBis);
        eintragenIntent.putExtra(Widget.KEY_PAUSE, mPause);
        if(position >= 0) {
            eintragenIntent.putExtra(Widget.KEY_DEFSCHICHT, schichtIDsListe[position]);
        }
        eintragenIntent.putExtra(Widget.KEY_ADDSCHICHT, sAdd.isChecked());

        sendBroadcast(eintragenIntent);
        finish();
    }
}