/* Copyright 2014-2023 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.ViewCompat;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASettings;
import askanimus.arbeitszeiterfassung2.setup.ISettings;

public class ZusatzwertAuswahlDialog implements
        ZusatzWerteAuswahlListAdapter.ZusatzwertAuswahlListeCallbacks,
        View.OnClickListener,
        SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {
    private final Context mContext;
    private final ZusatzwertAuswahlDialogCallbacks mCallBack;
    private final IZusatzfeld mZielFeld;
    private final ZusatzWertAuswahlListe mZusatzwertAuswahlListe;
    private final ZusatzWerteAuswahlListAdapter mZusatzwerteAuswahlListeAdapter;
    private final ImageView mSort_AZ;
    private final ImageView mSort_Benutzt;
    private final SearchView mSuche;
    private final AppCompatButton mAuswahlOK;
    private final Dialog mDialog;
    private final Arbeitsplatz mArbeitsplatz;

    public ZusatzwertAuswahlDialog(Context ctx, ZusatzwertAuswahlDialogCallbacks cb, IZusatzfeld zusatzfeld, Arbeitsplatz arbeitsplatz) {
        mContext = ctx;
        mCallBack = cb;
        mZielFeld = zusatzfeld;
        mArbeitsplatz = arbeitsplatz;

        // die Liste der möglichen Werte einlesen
        mZusatzwertAuswahlListe = new ZusatzWertAuswahlListe(zusatzfeld, mArbeitsplatz);
        // den Listadapter einrichten
        mZusatzwerteAuswahlListeAdapter = new ZusatzWerteAuswahlListAdapter(mZusatzwertAuswahlListe, mContext, this);

        // den Auswahldialog anlegen
        mDialog = new Dialog(ctx);
        mDialog.setTitle(R.string.zusatzwert_auswahl_titel);
        mDialog.setContentView(R.layout.fragment_zusatzwert_auswahl);

        // die zur Auswahl stehende Werteliste
        ListView mListView = mDialog.findViewById(R.id.auswahl_Liste);
        mListView.setAdapter(mZusatzwerteAuswahlListeAdapter);

        // einen neuen Eintrag anlegen
        AppCompatButton mListAdd = mDialog.findViewById(R.id.auswahl_Plusbutton);
        ViewCompat.setBackgroundTintList(mListAdd, mArbeitsplatz.getFarbe_Button());
        mListAdd.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());
        mListAdd.setOnClickListener(this);

        // keinen Eintrag auswählen
        AppCompatButton mListNo = mDialog.findViewById(R.id.auswahl_Nobutton);
        mListNo.setOnClickListener(this);

        // mehrere Einträge (nur Text) auswählen
        mAuswahlOK = mDialog.findViewById(R.id.auswahl_button);
        if(mZielFeld.getDatenTyp() == IZusatzfeld.TYP_AUSWAHL_TEXT) {
            mAuswahlOK.setOnClickListener(this);
            if(mZusatzwertAuswahlListe.getSummeSelectedItems() < 1) {
                mAuswahlOK.setVisibility(View.GONE);
            }
        } else {
            mAuswahlOK.setVisibility(View.GONE);
        }

        // Eintragsuche
        mSuche = mDialog.findViewById(R.id.auswahl_filter);
        mSuche.setQueryHint(mContext.getString(R.string.suche_zusatzwert_hint));
        mSuche.setIconified(false);
        mSuche.clearFocus();
        mSuche.setOnQueryTextListener(this);
        mSuche.setOnCloseListener(this);
        mZusatzwerteAuswahlListeAdapter.mFilterData = null;

        // Sortierfunktionen
        mSort_AZ = mDialog.findViewById(R.id.auswahl_sort_az);
        mSort_Benutzt = mDialog.findViewById(R.id.auswahl_sort_benutzt);
        mSort_AZ.setOnClickListener(this);
        mSort_Benutzt.setOnClickListener(this);
    }

    public void open(){
        if(mDialog != null)
            mDialog.show();
    }


    public void addWert(ZusatzWertAuswahlListe.zusatzWertAuswahlItem wert){
        mZusatzwertAuswahlListe.addWert(wert);
        mZusatzwertAuswahlListe.changeSummeItemSelected(wert.setSelect(true));
        mZusatzwerteAuswahlListeAdapter.notifyDataSetChanged();
        onAuswahlItem();
    }

    @Override
    public void onZusatzwerteChange() {
        String q = mSuche.getQuery().toString();
        mSuche.setQuery("", false);
        mSuche.setQuery(q, false);

        mZusatzwerteAuswahlListeAdapter.notifyDataSetChanged();
    }


    @Override
    public void onSelectItem(IZusatzfeld item) {
        mCallBack.onZusatzwertSet(item);
        mDialog.dismiss();
    }

    @Override
    public void onDeleteItem(int position){
        String sWert = mZusatzwertAuswahlListe.getEintragWert(position).getStringWert(true);
        new AlertDialog.Builder(mContext)
                .setTitle(mContext.getString(R.string.dialog_delete, sWert))
                .setMessage(mContext.getString(R.string.dialog_delete_frage, sWert))
                .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                    // Zusatzfeld löschen
                    mZusatzwertAuswahlListe.loescheEintrag(position);
                    mZusatzwerteAuswahlListeAdapter.notifyDataSetChanged();
                })
                .setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                    // Do nothing.
                }).show();
    }

    @Override
    public void onAuswahlItem(){
        if(mZielFeld.getDatenTyp() == IZusatzfeld.TYP_AUSWAHL_TEXT)
            mAuswahlOK.setVisibility(mZusatzwertAuswahlListe.getSummeSelectedItems()>=1?View.VISIBLE:View.GONE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.auswahl_Nobutton) {
            // die Anzahl der Verwendungen des/r ursprünglich ausgewählten Werte(s) verringern
            if(mZusatzwertAuswahlListe.getSummeSelectedItems() > 0) {
                for (ZusatzWertAuswahlListe.zusatzWertAuswahlItem item : mZusatzwertAuswahlListe.getListe()) {
                    item.setSelect(false);
                }
            }
            mZusatzwerteAuswahlListeAdapter.update();
            mCallBack.onZusatzwertSet(null);
            mDialog.dismiss();
        } else if (id == R.id.auswahl_sort_az) {
            ASettings.mPreferenzen.edit()
                    .putInt(
                            ISettings.KEY_SORT_AUSWAHLLISTE,
                            (
                                    ASettings.mPreferenzen.getInt(
                                            ISettings.KEY_SORT_AUSWAHLLISTE,
                                            ISettings.SORT_NO) == ISettings.SORT_AZ
                            ) ?
                                    ISettings.SORT_ZA :
                                    ISettings.SORT_AZ)
                    .apply();
            mZusatzwerteAuswahlListeAdapter.update();
            mZusatzwerteAuswahlListeAdapter.notifyDataSetChanged();
        } else if (id == R.id.auswahl_sort_benutzt) {
            ASettings.mPreferenzen.edit()
                    .putInt(ISettings.KEY_SORT_AUSWAHLLISTE, ISettings.SORT_BENUTZT)
                    .apply();
            mZusatzwerteAuswahlListeAdapter.update();
            mZusatzwerteAuswahlListeAdapter.notifyDataSetChanged();
        } else if (id == R.id.auswahl_Plusbutton) {
            mCallBack.onZusatzwertAdd(mZielFeld, new ZusatzWertAuswahlListe.zusatzWertAuswahlItem());
            if(mZielFeld.getDatenTyp() != IZusatzfeld.TYP_AUSWAHL_TEXT) {
                mDialog.dismiss();
            }
        } else if (id == R.id.auswahl_button) {
            StringBuilder werte = new StringBuilder();
            for (ZusatzWertAuswahlListe.zusatzWertAuswahlItem item : mZusatzwertAuswahlListe.getListe() ) {
                if(item.isSelect()){
                    if(werte.length() > 0){
                        werte.append(", ");
                    }
                    werte.append(item.getWert().getStringWert(false));
                }
            }
            Textfeld tf = new Textfeld(-1, -1, mZielFeld.getDefinition(), werte.toString());
            mCallBack.onZusatzwertSet(tf);
            mDialog.dismiss();
        }

    }

    // die Suchhandler
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mZusatzwerteAuswahlListeAdapter.getFilter().filter(newText);

        if (newText.isEmpty()) {
            mSort_AZ.setVisibility(View.VISIBLE);
            mSort_Benutzt.setVisibility(View.VISIBLE);
        } else {
            mSort_AZ.setVisibility(View.INVISIBLE);
            mSort_Benutzt.setVisibility(View.INVISIBLE);
        }
        return false;
    }

    @Override
    public boolean onClose() {
        mSort_AZ.setVisibility(View.VISIBLE);
        mSort_Benutzt.setVisibility(View.VISIBLE);
        return false;
    }

    public interface ZusatzwertAuswahlDialogCallbacks {
        /**
         * Aufgerufen wenn ein Wert ausgewählt wurde
         */
        void onZusatzwertSet(IZusatzfeld wert);
        void onZusatzwertAdd(IZusatzfeld zielfeld, ZusatzWertAuswahlListe.zusatzWertAuswahlItem eintragNeu);
    }
}
